﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class U9Button : Selectable, IEventSystemHandler, IPointerClickHandler, ISubmitHandler,IDragHandler,IPointerDownHandler, IPointerUpHandler {
	
	public System.Action OnClicked;
	public System.Action<Vector2,Vector2> OnDragged;
	public System.Action<Vector2> OnPressed;
	public System.Action<Vector2> OnReleased;

	public void Click()
	{
		if (OnClicked != null) {
			OnClicked ();
		}
	}
	
	public void OnPointerClick (PointerEventData eventData)
	{
		Click ();

	}

	public void OnSubmit (BaseEventData eventData)
	{
		Click ();
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (eventData.dragging && OnDragged != null)
			OnDragged (eventData.position, eventData.delta);		

	}


	public void OnPointerDown (PointerEventData eventData)
	{
		if (OnPressed != null) {
			OnPressed (eventData.position);
		}
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		if (OnReleased != null) {
			OnReleased (eventData.position);
		}
	}

}