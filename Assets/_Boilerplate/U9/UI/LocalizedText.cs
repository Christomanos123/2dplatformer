﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class LocalizedText : MonoBehaviour {

	[HideInInspector]
	[SerializeField]
	Text m_Label;

	[SerializeField]
	string m_StringKey;

	[SerializeField] bool m_ResizeRect = false;
	[SerializeField] bool m_ForceToUpper = false;

	public Text Label {
		get {
			return this.m_Label;
		}
	}

	void Reset() {
		m_Label = GetComponent<Text>();
	}

	void Start() {
		LocalizationManager.Instance.OnLocaleChanged += HandleLocalizationChanged;
		HandleLocalizationChanged ();
	}
		
	void HandleLocalizationChanged()
	{
		string text = LocalizationManager.Instance.Get (m_StringKey);
		if (m_ForceToUpper)
			text = text.ToUpper ();

		m_Label.text = text;

		if (m_ResizeRect)
			Resize ();
	}

	public void SetText(string text)
	{
		m_StringKey = text;
		HandleLocalizationChanged ();
	}

	[ContextMenu("Resize")]
	void Resize()
	{
		m_Label.rectTransform.sizeDelta = new Vector2 (m_Label.preferredWidth, m_Label.preferredHeight);
	}

	void OnDisable() {
		
	}
}
