﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Locale {
	en = 0,
	es = 1,
	ja = 2,
	zh_CN =3,
	de = 4,
	fr = 5,
	ru = 6,
	ar = 7,
	ko = 8
}

public enum LocaleName {
	English = 0,
	Spanish = 1,
	Japanese = 2,
	Chinese_Simplified = 3,
	German = 4,
	French = 5,
	Russian = 6,
	Arabic = 7,
	Korean = 8
}


public class LocalizationManager : MonoSingleton<LocalizationManager> {

	Dictionary<string,string> m_CopyDeck;

	public delegate void LocaleAction();
	public event LocaleAction OnLocaleChanged;

	[SerializeField] LocaleName m_DefaultLocale = LocaleName.English;

	// Use this for initialization
	void Awake () {
		Instance = this;
		ChangeLocale (m_DefaultLocale);
	}

	public void ChangeLocale(LocaleName localeName)
	{
		Locale locale = (Locale)(int)localeName;

		m_CopyDeck = new Dictionary<string, string> ();
		TextAsset copyJSON = Resources.Load<TextAsset> ("Localization/" + locale.ToString());

		object jsonObject = MiniJSON.jsonDecode (copyJSON.text);

        if (jsonObject.GetType() == typeof(Hashtable))
        {
            Hashtable jsonViewTable = jsonObject as Hashtable;

            foreach (DictionaryEntry view in jsonViewTable)
            {
                if (view.Value.GetType() == typeof(Hashtable))
                {
                    Hashtable jsonCopyTable = view.Value as Hashtable;
                    string viewKey = view.Key.ToString();

                    foreach (DictionaryEntry copyEntry in jsonCopyTable)
                    {
                        string key = viewKey + "." + copyEntry.Key.ToString();
                        string value = copyEntry.Value.ToString();
                        m_CopyDeck.Add(key, value);
                    }

                }
                else
                    Debug.LogError("Locale Error 2: Expecting Array");
            }

        }
        else
            Debug.LogError("Locale Error 1: Expecting Array");


        if (QAView.Instance != null)
			QAView.Instance.SetLocaleLabel (localeName, locale);

		if (OnLocaleChanged != null)
			OnLocaleChanged ();
	}
	
	public string Get(string id)
	{

		if (m_CopyDeck != null && m_CopyDeck.ContainsKey (id))
			return m_CopyDeck [id];
		else {
			Debug.LogWarning ("ID not found: " + id);
			return id;
		}
	}
}
