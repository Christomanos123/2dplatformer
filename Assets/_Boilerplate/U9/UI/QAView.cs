﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class QAView : MonoSingleton<QAView> {

	[SerializeField] GameObject m_Container;
	[SerializeField] Text m_BuildLabel;
	[SerializeField] Text m_UnityLabel;
	[SerializeField] Text m_LocaleLabel;
	[SerializeField] Text m_SceneLabel;
	[SerializeField] Text m_PlatformLabel;
	[SerializeField] Text m_DeviceLabel;
	[SerializeField] Text m_CurrentTimeLabel;
	[SerializeField] Text m_RuntimeLabel;

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		m_BuildLabel.text = "Build: " + Application.version;
		m_UnityLabel.text = "Unity: " + Application.unityVersion;
		m_PlatformLabel.text = "Platform: " + Application.platform.ToString ();
		m_DeviceLabel.text = "Device: " + SystemInfo.deviceModel;
	}

	public void SetLocaleLabel(LocaleName localeName, Locale localeCode)
	{
		string n = localeName.ToString ().Replace ('_', ' ');
		string c = localeCode.ToString ().ToUpper ().Replace('_','-');

		m_LocaleLabel.text = "Locale: " + n + " / " + c;
	}

	public void SetSceneLabel(string scene)
	{
		m_SceneLabel.text = "Scene: " + scene;
	}

	// Update is called once per frame
	void Update () {
		if (m_Container.activeSelf) {
			m_CurrentTimeLabel.text = "Current Time: " + DateTime.Now.ToString ("dd/MM/yyyy HH:mm:ss");

			float runTime = Time.time;

			int hours = Mathf.FloorToInt (runTime / 60f / 60f);
			int minutes = Mathf.FloorToInt (runTime / 60f);
			int seconds = Mathf.FloorToInt (runTime) % 60;
			int milliseconds = Mathf.FloorToInt ((runTime * 1000f) % 1000f);

			m_RuntimeLabel.text = string.Format ("Runtime: {0}:{1}:{2}.{3}", hours.ToString ("00"), minutes.ToString ("00"), seconds.ToString ("00"), milliseconds.ToString ("0000"));
		}
		if (Input.GetKeyDown (KeyCode.Tab))
			m_Container.SetActive (!m_Container.activeSelf);
	}
}
