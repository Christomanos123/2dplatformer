// View.cs
// 
// Created by Nick McVroom-Amoakohene <nicholas@unit9.com> on 07/09/2012.
// Based on code by David Rzepa <dave@unit9.com>
// Upgraded to DoTween by Andrew Oaten <andrew@unit9.com>
// Copyright (c) 2012 unit9 ltd. www.unit9.com. All rights reserved

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class U9View : MonoBehaviour {

	public enum ViewState {
		Displayed,
		Hidden,
		Displaying,
		Hiding
	}


	//----------------------------------------------------------------------------------------------------------------------------------//
	// Variables
	//----------------------------------------------------------------------------------------------------------------------------------//

	[Header("View Settings")]
	[SerializeField]	bool 		m_HideOnInit 				= true;		// Should this view be hidden when Initiated?
	[SerializeField] 	bool		m_DisableGameobjectOnHide 	= false;
	[SerializeField]	Canvas 		m_CanvasToDisableOnHide;				// The canvas to enable/disable
	[SerializeField]	U9Tween[]	m_DisplayTweens;
	[SerializeField]	U9Tween[]	m_HideTweens;

	bool 		m_Inited;										// Has the view been initiated?
	bool 		m_InteractionEnabled 	= true;					// Is interaction enabled on this view?
	protected ViewState 	m_State 				= ViewState.Hidden;		// The current display state of this view.
	Tween		m_LastTween				= null;

	/// <summary>
	/// An event that is triggered when the display state of this viw changes.
	/// </summary>
	public event System.Action<ViewState> StateChanged;	


	//----------------------------------------------------------------------------------------------------------------------------------//
	// Propeties & Getters
	//----------------------------------------------------------------------------------------------------------------------------------//

	/// <summary>
	/// The current display state of this view.
	/// </summary>
	public ViewState State {
		get {	
			return this.m_State; 	
		}
		private set {
			m_State = value;
			if (StateChanged != null) {
				StateChanged (value);
			}
		}
	}
	
	/// <summary>
	/// Is this view displayed, or in the process of being displayed?
	/// </summary>
	public bool IsDisplaying {
		get {
			return State == ViewState.Displayed || State == ViewState.Displaying;
		}
	}

	/// <summary>
	/// Is this view displayed, 
	/// </summary>
	public bool IsDisplayed {
		get {
			return State == ViewState.Displayed;
		}
	}

    /// <summary>
	/// Is this view displayed, or in the process of being displayed?
	/// </summary>
	public bool IsHiding
    {
        get
        {
            return State == ViewState.Hidden || State == ViewState.Hiding;
        }
    }

    /// <summary>
    /// Is this view displayed, 
    /// </summary>
    public bool IsHidden
    {
        get
        {
            return State == ViewState.Hidden;
        }
    }


    /// <summary>
    /// Has this view been initiated?
    /// </summary>
    public bool IsInited {
		get {
			return m_Inited;
		}
	}


	//----------------------------------------------------------------------------------------------------------------------------------//
	// Init
	//----------------------------------------------------------------------------------------------------------------------------------//

	protected virtual void Awake() {
		if( !m_Inited ) {
			InitView();
		}
	}

	/// <summary>
	/// Inits the view.
	/// </summary>
	public virtual void InitView() {
		m_Inited = true;

		for (int i = 0, ni = m_DisplayTweens.Length; i < ni; i++)
			m_DisplayTweens [i].Init ();

		for (int i = 0, ni = m_HideTweens.Length; i < ni; i++)
			m_HideTweens [i].Init ();

		if( m_HideOnInit ) {
			Hide();
		} else {
			Display();
		}		
	}

	/// <summary>
	/// Inits the view.
	/// </summary>
	public void AttemptInitView() {
		if( !m_Inited ) {
			InitView();
		}
	}

	void ResetTweens() {
		if (m_Inited) {
			for (int i = 0, ni = m_DisplayTweens.Length; i < ni; i++)
				m_DisplayTweens [i].Unapply ();

			for (int i = 0, ni = m_HideTweens.Length; i < ni; i++)
				m_HideTweens [i].Unapply ();
		}
	}

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Display
	//----------------------------------------------------------------------------------------------------------------------------------//

	/// <summary>
	/// Displays the view instantly.
	/// </summary>
	public virtual void Display() {
		AttemptInitView ();

		KillLastTween ();
		ResetTweens ();
		BeginDisplay();
		EndDisplay();
	}


	/// <summary>
	/// Creates a DoTween Sequence that displays the view, and assigns to it the listeners.
	/// </summary>
	public virtual Tween GetDisplayTween( int displayIndex = 0 ) {
		
		if(IsDisplaying || m_DisplayTweens.Length<= displayIndex) {
			return m_LastTween = DOTween.Sequence();
		} else {
			ResetTweens ();
			
			m_State = ViewState.Displaying;

			Tween t = CreateDisplayTween(displayIndex);
			AddDisplayListeners( t );
			return m_LastTween = t;
		}
	}

	protected virtual Tween CreateDisplayTween( int displayIndex = 0 ) {
		U9Tween displayTweener = m_DisplayTweens [displayIndex];
		displayTweener.Apply ();

		return displayTweener.GetTween (true);
	}

	/// <summary>
	/// Adds event listeners to the display sequence.
	/// </summary>
	protected void AddDisplayListeners( Tween displayTween ) {
		displayTween.OnStart (HandleDisplayTweenBegan);
		displayTween.OnComplete(HandleDisplayTweenEnded);
		displayTween.OnKill (HandleDisplayTweenKilled);
	}	


	/// <summary>
	/// Handles when the display sequence has began.
	/// </summary>
	protected virtual void HandleDisplayTweenBegan(){
		BeginDisplay();
	}


	/// <summary>
	/// Handles when the display sequence has ended.
	/// </summary>
	protected virtual void HandleDisplayTweenEnded( ){
		EndDisplay ();
	}

	/// <summary>
	/// Handles when the display sequence has been killed.
	/// </summary>
	protected virtual void HandleDisplayTweenKilled() {
		//EndDisplay ();
	}


	/// <summary>
	/// Display has begun, trigger related events and properties.
	/// </summary>
	protected virtual void BeginDisplay() {
		DisableInteraction ();

		if (m_DisableGameobjectOnHide)
			gameObject.SetActive (true);

		if( m_CanvasToDisableOnHide != null ) {
			m_CanvasToDisableOnHide.enabled = true;
		}
		
		State = ViewState.Displaying;	

	}


	/// <summary>
	/// The display has finished.
	/// </summary>
	protected virtual void EndDisplay() {
		State = ViewState.Displayed;
		EnableInteraction ();
	}


	//----------------------------------------------------------------------------------------------------------------------------------//
	// Variables
	//----------------------------------------------------------------------------------------------------------------------------------//

	/// <summary>
	/// Hides this view instantly.
	/// </summary>
	public virtual void Hide() { 
		AttemptInitView ();
		KillLastTween ();
		ResetTweens ();
		BeginHide();
		EndHide();
	}


	/// <summary>
	/// Creates a DoTween Sequence that hides the view, and assigns to it the listeners.
	/// </summary>
	public virtual Tween GetHideTween( int hideIndex = 0 ) {
		if(!IsDisplaying || m_HideTweens.Length<= hideIndex) {
			return  m_LastTween = DOTween.Sequence();
		} else {
			ResetTweens ();

			m_State = ViewState.Hiding;

			Tween t = CreateHideTween(hideIndex);
			AddHideListeners( t );
			return m_LastTween = t;
		}
	}

	protected virtual Tween CreateHideTween( int hideIndex = 0 ) {
		U9Tween hideTweener = m_HideTweens [hideIndex];

		return hideTweener.GetTween();
	}


	/// <summary>
	/// Adds event listeners to the hide sequence.
	/// </summary>
	protected void AddHideListeners( Tween hideTween ) {
		hideTween.OnStart (HandleHideTweenBegan);
		hideTween.OnComplete(HandleHideTweenEnded);
		hideTween.OnKill(HandleHideTweenKilled);
	}


	/// <summary>
	/// Handles when the hide sequence has began.
	/// </summary>
	protected virtual void HandleHideTweenBegan ()
	{
		BeginHide();
	}


	/// <summary>
	/// Handles when the hide sequence has ended.
	/// </summary>
	protected virtual void HandleHideTweenEnded() {
		EndHide ();
	}


	/// <summary>
	/// Handles when the display sequence has been killed.
	/// </summary>
	protected virtual void HandleHideTweenKilled() {
		//EndHide ();
	}


	/// <summary>
	/// Hide has begun, trigger related events and properties.
	/// </summary>
	protected virtual void BeginHide() {
		
		DisableInteraction ();

		State = ViewState.Hiding;
	}


	/// <summary>
	/// The hide has finished.
	/// </summary>
	protected virtual void EndHide() {
		State = ViewState.Hidden;

		if (m_DisableGameobjectOnHide)
			gameObject.SetActive (false);

		if( m_CanvasToDisableOnHide != null ) {
			m_CanvasToDisableOnHide.enabled = false;
		}
	}

	public void KillLastTween()
	{
		if (m_LastTween != null)
			m_LastTween.Kill ();
	}

	//----------------------------------------------------------------------------------------------------------------------------------//
	// Interaction Status
	//----------------------------------------------------------------------------------------------------------------------------------//

	/// <summary>
	/// Disables the interaction on this view.
	/// </summary>
	public void DisableInteraction() {
		m_InteractionEnabled = false;
		DisableInteractionInternal ();
	}

	/// <summary>
	/// Enables the interaction on this view.
	/// </summary>
	public void EnableInteraction() {
		m_InteractionEnabled = true;
		EnableInteractionInternal ();
	}

	protected virtual void EnableInteractionInternal() {}
	protected virtual void DisableInteractionInternal() {}
}
