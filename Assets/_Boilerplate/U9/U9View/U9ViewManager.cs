﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9ViewManager : MonoSingleton<U9ViewManager> {

    


	U9View m_CurrentView;
	U9View m_PreviousView;
	Sequence m_CurrentTween;

	public U9View CurrentView {
		get {
			return m_CurrentView;
		}
		set {
			m_CurrentView = value;
		}
	}

	public U9View PreviousView {
		get {
			return m_PreviousView;
		}
		set {
			m_PreviousView = value;
		}
	}


    void Awake()
	{
		Instance = this;
	}
		
	public void KillSequence()
	{
		if (m_CurrentTween != null)
			m_CurrentTween.Kill ();

		m_CurrentTween = null;
	}

	public Sequence GetSwitchSequence (U9View newView, bool ignoreTimeScale = false)
	{
		return GetSwitchSequence (newView, CompositionType.Serial, 0, 0, 0f).SetUpdate(ignoreTimeScale);
	}

	public Sequence GetSwitchSequence (U9View newView, CompositionType composition, bool ignoreTimeScale = false)
	{
		return GetSwitchSequence (newView, composition, 0, 0,  0.1f).SetUpdate(ignoreTimeScale);
	}

	public Sequence GetSwitchSequence (U9View newView, CompositionType composition, float offset, bool ignoreTimeScale = false)
	{
		return GetSwitchSequence (newView, composition, 0, 0, offset).SetUpdate(ignoreTimeScale);
	}

	public Sequence GetSwitchSequence (U9View newView, int displayTweenID, int hideTweenID, bool ignoreTimeScale = false)
	{
		return GetSwitchSequence (newView, CompositionType.Serial, displayTweenID, hideTweenID, 0f).SetUpdate(ignoreTimeScale);
	}

	public Sequence GetSwitchSequence (U9View newView, CompositionType composition, int displayTweenID, int hideTweenID, float offset, bool ignoreTimeScale = false)
	{
		KillSequence ();

		m_CurrentTween = DOTween.Sequence();

		if (m_CurrentView != null) {
			Debug.Log ("Hiding: " + m_CurrentView.name);
			m_CurrentTween.Append (m_CurrentView.GetHideTween (hideTweenID));
		}
		else
			offset = 0;

		if (newView != null) {
			Debug.Log ("Displaying: " + newView.name);

			switch (composition) {
			case CompositionType.Parallel:
				m_CurrentTween.Join (newView.GetDisplayTween (displayTweenID));
				break;
			case CompositionType.Serial:
				m_CurrentTween.Append (newView.GetDisplayTween (displayTweenID));
				break;
			case CompositionType.Stagger:
			default:
				m_CurrentTween.Insert (offset,newView.GetDisplayTween (displayTweenID));
				break;
			}
		}

		m_PreviousView = m_CurrentView;
		m_CurrentView = newView;

		return m_CurrentTween.SetUpdate(ignoreTimeScale);
	}
}
