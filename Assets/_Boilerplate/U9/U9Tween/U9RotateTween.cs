﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9RotateTween : U9Tween {

	[SerializeField] Vector3 m_TargetRotation;

	Vector3 	m_DefaultRotation;

	protected override void InitParameters ()
	{
		m_DefaultRotation = transform.localEulerAngles;
	}

	protected override Tween CreateTween ()
	{
		Tween t = transform.DOLocalRotate (m_TargetRotation, m_Duration);
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = transform.DOLocalRotate (m_DefaultRotation, m_Duration);
		return t;
	}

	protected override void UnapplyTweener()
	{
		transform.localEulerAngles = m_DefaultRotation;
	}

	protected override void ApplyTweener()
	{
		transform.localEulerAngles = m_TargetRotation;
	}
}
