﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9ScaleTween : U9Tween {

	[SerializeField] Vector3 m_TargetScale;

	Vector3 	m_DefaultScale;

	protected override void InitParameters ()
	{
		m_DefaultScale = transform.localScale;

		if( m_TargetScale == Vector3.zero ) {
			m_TargetScale = new Vector3(0.0001f,0.0001f,0.0001f);
		}
	}

	protected override Tween CreateTween ()
	{
		Tween t = transform.DOScale (m_TargetScale, m_Duration);
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = transform.DOScale (m_DefaultScale, m_Duration);
		return t;
	}

	protected override void UnapplyTweener()
	{
		transform.localScale = m_DefaultScale;
	}

	protected override void ApplyTweener()
	{
		transform.localScale = m_TargetScale;
	}
}
