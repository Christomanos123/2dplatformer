﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class U9Tween : MonoBehaviour {

	[SerializeField] protected	string 	m_ID				= "Tween";
	[SerializeField] protected 	float 	m_Duration 			= 0.35f;
	[SerializeField] 			Ease 	m_EaseType 			= Ease.InOutQuad;	
	[SerializeField] 			bool	m_IgnoreTimeScale 	= false;

	Tween 	m_CurrentTween;
	bool 	m_Initted = false;

	void Awake()
	{
		
	}

	public void Init()
	{
		if (m_Initted)
			return;

		InitParameters ();

		m_Initted = true;
	}

	protected abstract void InitParameters ();

	public void Kill()
	{
		if (m_CurrentTween != null)
			m_CurrentTween.Kill ();
		m_CurrentTween = null;
	}

	public Tween GetTween (bool reverse = false)
	{
		Init ();

		Tween t = reverse ? CreateReverseTween() : CreateTween ();
		t.SetUpdate (m_IgnoreTimeScale);
		t.SetEase (m_EaseType);
		t.SetId (m_ID);

		return t;
	}

	protected abstract Tween CreateTween ();
	protected abstract Tween CreateReverseTween ();


	public void Play(bool reverse = false)
	{
		Kill ();
		m_CurrentTween = GetTween (reverse);
		m_CurrentTween.Play ();
	}

	public void Rewind()
	{
		Kill ();
		m_CurrentTween = GetTween (true);
		m_CurrentTween.Play ();
	}

	public void Unapply()
	{
		if (!m_Initted)
			return;

		Kill ();
		UnapplyTweener ();
	}

	protected abstract void UnapplyTweener();

	public void Apply()
	{
		Init ();
		ApplyTweener ();
	}

	protected abstract void ApplyTweener();

	#if UNITY_EDITOR

	[ContextMenu("Play Tween")]
	protected void TestPlay()
	{
		if (Application.isPlaying) {
			//Play ();
			Invoke ("InternalTestPlay", 0.5f);
		}
	}

	[ContextMenu("Play Tween in Reverse")]
	protected void TestReversePlay()
	{
		if(Application.isPlaying)
			Invoke ("InternalTestReversePlay", 0.5f);
	}

	protected void InternalTestPlay()
	{
		Play ();
	}

	protected void InternalTestReversePlay()
	{
		Play (true);
	}

	[ContextMenu("Apply")]
	protected void TestApply()
	{
		if(Application.isPlaying)
			Apply ();
	}

	[ContextMenu("Unapply")]
	protected void TestUnapply()
	{
		if(Application.isPlaying)
			Apply ();
	}


	#endif
}
