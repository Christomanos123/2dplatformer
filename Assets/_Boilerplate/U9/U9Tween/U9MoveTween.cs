﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9MoveTween : U9Tween {

	[SerializeField] Vector3 m_TargetPosition;

	Vector3 	m_DefaultPosition;

	protected override void InitParameters ()
	{
		m_DefaultPosition = transform.localPosition;
	}

	protected override Tween CreateTween ()
	{
		Tween t = transform.DOLocalMove (m_TargetPosition, m_Duration);
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = transform.DOLocalMove (m_DefaultPosition, m_Duration);
		return t;
	}

	protected override void UnapplyTweener()
	{
		transform.localPosition = m_DefaultPosition;
	}

	protected override void ApplyTweener()
	{
		transform.localPosition = m_TargetPosition;
	}
}
