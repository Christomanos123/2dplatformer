﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9RectDimensionsTween : U9Tween {

	[SerializeField] Vector2 m_TargetDimensions;

	Vector2 	m_DefaultDimensions;
	RectTransform m_RectTransform;
	float m_CurrentLerp;

	protected override void InitParameters ()
	{
		m_RectTransform = (RectTransform)transform;
		m_DefaultDimensions = m_RectTransform.sizeDelta;
		m_CurrentLerp = 0;
	}

	protected override Tween CreateTween ()
	{
		Tween t = DOTween.To (UpdateDimensions, m_CurrentLerp, 1, m_Duration);;
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = DOTween.To (UpdateDimensions, m_CurrentLerp, 0, m_Duration);
		return t;
	}

	void UpdateDimensions(float l)
	{
		m_RectTransform.sizeDelta = Vector2.Lerp (m_DefaultDimensions, m_TargetDimensions, l);
		m_CurrentLerp = l;
	}

	protected override void UnapplyTweener()
	{
		m_RectTransform.sizeDelta = m_DefaultDimensions;
		m_CurrentLerp = 0;
	}

	protected override void ApplyTweener()
	{
		m_RectTransform.sizeDelta = m_TargetDimensions;
		m_CurrentLerp = 1;
	}
}
