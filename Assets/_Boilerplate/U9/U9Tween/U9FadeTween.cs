﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9FadeTween : U9Tween {

	[SerializeField] float m_TargetAlpha;
	[SerializeField] CanvasGroup m_Fadable;

	float m_DefaultAlpha;

	protected override void InitParameters ()
	{
		if(m_Fadable != null)
			m_DefaultAlpha = m_Fadable.alpha;
	}

	protected override Tween CreateTween ()
	{
		Tween t;

		if (m_Fadable != null)
			t = m_Fadable.DOFade (m_TargetAlpha, m_Duration);
		else
			t = DOTween.Sequence ();
		
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t;

		if (m_Fadable != null)
			t = m_Fadable.DOFade (m_DefaultAlpha, m_Duration);
		else
			t = DOTween.Sequence ();

		return t;
	}

	protected override void UnapplyTweener()
	{
		if(m_Fadable != null)
			m_Fadable.alpha = m_DefaultAlpha;
	}

	protected override void ApplyTweener()
	{
		if(m_Fadable != null)
			m_Fadable.alpha = m_TargetAlpha;
	}
}
