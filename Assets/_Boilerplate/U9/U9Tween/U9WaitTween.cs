﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9WaitTween : U9Tween {

	protected override void InitParameters ()
	{
	}

	protected override Tween CreateTween ()
	{
		Tween t = DOTween.Sequence().AppendInterval(m_Duration);
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = DOTween.Sequence().AppendInterval(m_Duration);
		return t;
	}

	protected override void UnapplyTweener()
	{
	}

	protected override void ApplyTweener()
	{
	}
}
