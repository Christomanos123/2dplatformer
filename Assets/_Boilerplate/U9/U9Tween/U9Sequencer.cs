﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum CompositionType {
	Parallel,
	Serial,
	Stagger
}

public class U9Sequencer : U9Tween {
	
	[SerializeField] CompositionType 	m_CompositionType;
	[SerializeField] float				m_StaggerAmount = 0.1f;	
	[SerializeField] U9Tween[] 			m_Tweeners;
	[SerializeField] float 				m_Delay = 0;

	Vector3 	m_DefaultRotation;

	protected override void InitParameters ()
	{
		for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
			m_Tweeners [i].Init ();
		}
	}

	protected override Tween CreateTween ()
	{
		Sequence s = DOTween.Sequence ();

		switch (m_CompositionType) {
		case CompositionType.Parallel:

			for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
				s.Join (m_Tweeners [i].GetTween ());
			}
			break;

		case CompositionType.Serial:
			for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
				s.Append (m_Tweeners [i].GetTween ());
			}
			break;

		case CompositionType.Stagger:
			float currentStagger = 0;

			for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
				s.Insert (currentStagger, m_Tweeners [i].GetTween ());
				currentStagger += m_StaggerAmount;
			}
			break;
		default:
			break;
		}

		if (m_Delay > 0)
			s.PrependInterval (m_Delay);

		return s;
	}

	protected override Tween CreateReverseTween ()
	{
		Sequence s = DOTween.Sequence ();

		switch (m_CompositionType) {
		case CompositionType.Parallel:

			for (int i = m_Tweeners.Length-1; i>=0; i--) {
				s.Join (m_Tweeners [i].GetTween (true));
			}
			break;

		case CompositionType.Serial:
			for (int i = m_Tweeners.Length-1; i>=0; i--) {
				s.Append (m_Tweeners [i].GetTween (true));
			}
			break;

		case CompositionType.Stagger:
			float currentStagger = 0;

			for (int i = m_Tweeners.Length-1; i>=0; i--) {
				s.Insert (currentStagger, m_Tweeners [i].GetTween (true));
				currentStagger += m_StaggerAmount;
			}
			break;
		default:
			break;
		}

		if (m_Delay > 0)
			s.PrependInterval (m_Delay);
		
		return s;
	}

	protected override void UnapplyTweener()
	{
		for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
			m_Tweeners [i].Unapply ();
		}
	}

	protected override void ApplyTweener()
	{
		for (int i = 0, ni = m_Tweeners.Length; i < ni; i++) {
			m_Tweeners [i].Apply ();
		}
	}

	[ContextMenu("Reverse List")]
	public void ReverseList()
	{
		U9Tween[] reversedTweeners =new U9Tween[m_Tweeners.Length];

		for (int i = 0, ni = m_Tweeners.Length; i < ni; i++)
			reversedTweeners [i] = m_Tweeners [ni - 1 - i];

		m_Tweeners = reversedTweeners;
	}
}

