﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class U9RectMoveTween : U9Tween {

	[SerializeField] Vector2 m_Offset;

	Vector2 	m_DefaultPosition;
	RectTransform m_RectTransform;
	float m_CurrentLerp;



	protected override void InitParameters ()
	{
		m_RectTransform = (RectTransform)transform;
		m_DefaultPosition = m_RectTransform.anchoredPosition;
		m_CurrentLerp = 0;
	}

	protected override Tween CreateTween ()
	{
		Tween t = DOTween.To (UpdateMove, m_CurrentLerp, 1, m_Duration);;
		return t;
	}

	protected override Tween CreateReverseTween ()
	{
		Tween t = DOTween.To (UpdateMove, m_CurrentLerp, 0, m_Duration);
		return t;
	}

	void UpdateMove(float l)
	{
		m_RectTransform.anchoredPosition = Vector2.Lerp (m_DefaultPosition, m_DefaultPosition+m_Offset, l);
		m_CurrentLerp = l;
	}

	protected override void UnapplyTweener()
	{
		m_RectTransform.anchoredPosition = m_DefaultPosition;
		m_CurrentLerp = 0;
	}

	protected override void ApplyTweener()
	{
		m_RectTransform.anchoredPosition = m_DefaultPosition + m_Offset;
		m_CurrentLerp = 1;
	}
}
