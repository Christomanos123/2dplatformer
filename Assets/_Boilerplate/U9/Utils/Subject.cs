using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

public class Subject<T> {
	
	T currentValue;
	CompositionType m_CompositionType;	

	public Subject () : this( default(T) )
	{
		
	}
	
	public Subject (T value)
	{
		this.currentValue = value;
		this.m_CompositionType = CompositionType.Serial;
	}

	public Subject (T value, CompositionType compositionType)
	{
		this.currentValue = value;
		this.m_CompositionType = compositionType;
	}


	public class SubjectChangedEventArgs : System.EventArgs {

		public SubjectChangedEventArgs() {
			Sequences = new List<Sequence>();
		}

		public List<Sequence> Sequences { get; internal set; }
		public T OldValue { get; internal set; }
		public T NewValue { get; internal set; }
		public bool IsInitialEvent { get; internal set; }
	}

	private event System.EventHandler<SubjectChangedEventArgs> ChangedInternal;

	public event System.EventHandler<SubjectChangedEventArgs> Changed {
		add {
			ChangedInternal += value;
			SubjectChangedEventArgs e = new SubjectChangedEventArgs () { OldValue = currentValue, NewValue = currentValue, IsInitialEvent = true };		
			value ( this, e);		

			Sequence subjectSequence = CreateCompositeSequence(e.Sequences.ToArray());
			subjectSequence.Play();
		}		
		remove {
			ChangedInternal -= value;
		}
	}

	void OnChanged( SubjectChangedEventArgs e ) {
		if(ChangedInternal !=null)
			ChangedInternal (this, e);
	}

	public T GetValue() {
		return currentValue;
	}

	public void SetValue( T newValue ) {
		Sequence sequence;
		SetValue (newValue, out sequence );

		sequence.Play();
	}

	public void SetValue( T newValue, out Sequence sequence ) {
		T oldValue = currentValue;
		currentValue = newValue;

		SubjectChangedEventArgs e = new SubjectChangedEventArgs () { OldValue = oldValue, NewValue = newValue, IsInitialEvent = false };

		OnChanged(e);

		sequence = CreateCompositeSequence(e.Sequences.ToArray());
	}


	Sequence CreateCompositeSequence( Sequence[] ts ) {
		
		Sequence compositedSequence = DOTween.Sequence ();
		
		switch( m_CompositionType ) {
		case CompositionType.Parallel:
			foreach(Sequence s in ts)
				compositedSequence.Join(s);
			break;
			
		case CompositionType.Serial:
			foreach(Sequence s in ts)
				compositedSequence.Append(s);
			break;
			
		case CompositionType.Stagger:
			float currentStagger = 0;
			
			foreach(Sequence s in ts)
			{
				compositedSequence.Insert(currentStagger,s);
				currentStagger+= 0.1f;
			}
			break;
		default:
			break;
		}
		
		return compositedSequence;
	}
	
}
