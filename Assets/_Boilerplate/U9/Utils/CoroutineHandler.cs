﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineHandler : MonoBehaviour {

    static CoroutineHandler instance;

    public static CoroutineHandler Instance
    {
        get
        {
            if(instance == null)
            {
                GameObject go = new GameObject("CoroutineHandler");
                instance = go.AddComponent<CoroutineHandler>();
            }

            return instance;
        }
        
    }



    // Use this for initialization
    void Start () {
        instance = this;
	}
	
    
}
