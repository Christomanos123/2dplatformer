﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

#if UNITY_IPHONE
using System.Runtime.InteropServices;
#endif

public class ShareController : MonoSingleton<ShareController> {

	string m_DefaultSubject = "Share.Subject";
	string m_DefaultTitle = "Share.Title";
	string m_DefaultContent = "Share.Content";
	string m_DefaultURL = "Share.URL";

	string m_FacebookPackage = "com.facebook.katana";
	string m_TwitterPackage = "com.twitter.android";

	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	public void ShareFacebook()
	{
		string newPath = CreateIconPath ();

		LocalizationManager lm = LocalizationManager.Instance;

		string subject = lm.Get (m_DefaultSubject);
		string title = lm.Get (m_DefaultTitle);
		string content = lm.Get (m_DefaultContent);
		string url = lm.Get (m_DefaultURL);

		#if UNITY_ANDROID
		try
		{
			//Debug.Log("Attempt share by Facebook");
			AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
			AndroidJavaObject intent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", m_FacebookPackage);

			if(intent != null)
			{
				//Debug.Log("Facebook found");
				ShareAndroid(subject, title, content,newPath,false,m_FacebookPackage);
			}
			else
				Share (subject, title, content,url,newPath);
		}
		catch
		{
			//Debug.Log("Facebook not found");
			Share (subject, title, content,url,newPath);
		}

		#elif UNITY_IOS
		CallFacebookShare (content, subject, url,newPath);
		#endif



	}

	public void ShareTwitter()
	{
		string newPath = CreateIconPath ();

		LocalizationManager lm = LocalizationManager.Instance;
		string subject = lm.Get (m_DefaultSubject);
		string title = lm.Get (m_DefaultTitle);
		string content = lm.Get (m_DefaultContent);
		string url = lm.Get (m_DefaultURL);

		#if UNITY_ANDROID
		try
		{
			//Debug.Log("Attempt share by Twitter");
			AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
			AndroidJavaObject intent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", m_TwitterPackage);

			if(intent != null)
			{
				//Debug.Log("Twitter found");
				ShareAndroid(subject, title, content,newPath,false,m_TwitterPackage);
			}
			else
				Share (subject, title, content,url,newPath);
		}
		catch
		{
			//Debug.Log("Twitter not found");
			Share (subject, title, content,url,newPath);
		}
		#elif UNITY_IOS
		CallTwitterShare (content, subject, url,newPath);
		#endif



	}

	public void Share()
	{
		string newPath =CreateIconPath ();

		LocalizationManager lm = LocalizationManager.Instance;
		string subject = lm.Get (m_DefaultSubject);
		string title = lm.Get (m_DefaultTitle);
		string content = lm.Get (m_DefaultContent);
		string url = lm.Get (m_DefaultURL);

		Share (subject, title, content,url, newPath);
	}

	public void ShareAndroid(string subject, string title,string content,string newPath, bool useChooser = true,string application = "")
	{
		#if UNITY_ANDROID
		//Debug.Log("Create Intent");
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		// Set action for intent
		//Debug.Log("Set Action");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		intentObject.Call<AndroidJavaObject>("setType", "text/plain");

		//Set Subject of action
		//Debug.Log("Set Extras");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);

		//Set title of action or intent
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), title);

		// Set actual data which you want to share
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"),content);

		//Debug.Log("Get Activity");
		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		// Invoke android activity for passing intent to share data
		if(useChooser)
		{
			//Debug.Log("Get Chooser");
			AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
			currentActivity.Call("startActivity", jChooser);
		}
		else
		{
			//Debug.Log("Open: " + application);
			intentObject.Call<AndroidJavaObject>("setPackage", application);
			currentActivity.Call("startActivity", intentObject);
		}

		#endif
	}


	public void Share(string subject, string title, string content, string url, string newPath)
	{


#if UNITY_ANDROID

		ShareAndroid(subject,title,content,newPath);

#elif UNITY_IOS
		CallSocialShareAdvanced(content, subject,url,newPath);
#endif

	}

	string CreateIconPath()
	{
		string newPath = Application.persistentDataPath + "/playstore-icon.png";

		if (!File.Exists (newPath)) {
			Texture2D image = Resources.Load<Texture2D> ("playstore-icon");
			byte[] bytes = image.EncodeToPNG();
			File.WriteAllBytes (newPath, bytes);
		}

		return newPath;
	}

#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}


	[DllImport ("__Internal")] private static extern void showFacebookSharing(ref SocialSharingStruct conf);

	public static void CallFacebookShare(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showFacebookSharing(ref conf);
	}

	[DllImport ("__Internal")] private static extern void showTwitterSharing(ref SocialSharingStruct conf);

	public static void CallTwitterShare(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;


		showTwitterSharing(ref conf);
	}
#endif


}

