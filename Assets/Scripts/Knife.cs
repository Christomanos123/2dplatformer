﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class Knife : MonoBehaviour {

    [SerializeField]
    private float speed;

    private Vector2 direction;

    private Rigidbody2D myRigidBody2D;
    
	// Use this for initialization
	void Start ()
    {
        myRigidBody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        myRigidBody2D.velocity = direction * speed;
	}

    public void Initialize(Vector2 direction)
    {
        this.direction = direction;
    }



    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
