﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateStuff;

public class PatrolStateEnemy : State<Enemy>
{
    private static PatrolStateEnemy _instance;
    private Enemy enemy;
    private float patrolTimer;
    private float patrolDuration;

    private PatrolStateEnemy()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PatrolStateEnemy Instance
    {
        get
        {
            if (_instance != null)
            {
                new PatrolStateEnemy();
            }

            return _instance;
        }
    }

    public override void EnterState(Enemy _owner)
    {
        patrolDuration = UnityEngine.Random.Range(1, 10);
        
    }

    public override void ExitState(Enemy _owner)
    {
       
    }

    public override void UpdateState(Enemy _owner)
    {
        Patrol(_owner);
        _owner.Move();

        if (_owner.Target != null && _owner.InThrowRange)
        {
            //enemy.ChangeState(new RangedState());
        }
    }

    private void Patrol(Enemy _owner)
    {
        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolDuration)
        {
          //  _owner.stateMachine.ChangeState(IdleStateEnemy.Instance);
        }
    }

    public void OnTriggerEnter(Collider2D other)
    {
        if (other.CompareTag("Knife") || other.CompareTag("Sword"))
        {
            enemy.Target = Player.Instance.gameObject;
        }

    }

}
