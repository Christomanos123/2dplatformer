﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateStuff;

public class IdleStateEnemy : State<Enemy>
{
    private static IdleStateEnemy _instance;

    private Enemy enemy;
    private float idleTimer;
    private float idleDuration;


    private IdleStateEnemy()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }
    public static IdleStateEnemy Instance
    {
        get
        {
            if (_instance != null)
            {
                new IdleStateEnemy();
            }

            return _instance;
        }
    }

    public override void EnterState(Enemy _owner)
    {
        idleDuration = UnityEngine.Random.Range(1, 10);
        enemy = _owner;
    }

    public override void UpdateState(Enemy _owner)
    {
        Idle(_owner);

        if (_owner.Target != null)
        {
           // _owner.stateMachine.ChangeState(PatrolStateEnemy.Instance);
        }
    }

    public override void ExitState(Enemy _owner)
    {
        
    }


    private void Idle(Enemy _owner)
    {
        //When we are idling: 1) we need to stop moving 2) Start the timer that we can wait 3) change to patrol state
        _owner.MyAnimator.SetFloat("speed", 0);
        idleTimer += Time.deltaTime;

        if (idleTimer >= idleDuration)
        {
           // _owner.stateMachine.ChangeState(PatrolStateEnemy.Instance);
        }
    }

    public void OnTriggerEnter(Collider2D other)
    {
        //This will acquire a target for enemy even if it is out of sight.
        //If we get hit by a knife, our target is now the player.
        if (other.CompareTag("Knife") || other.CompareTag("Sword"))
        {
            enemy.Target = Player.Instance.gameObject;
        }
    }
}
