﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

public class CreateLevel : EditorWindow
{
    Object tree_0;
    Object tree_1;
    Object rock;

    Object floor_TopLeft;
    Object floor_TopFiller;
    Object floor_TopRight;

    Object floor_LeftFiller;
    Object floor_MiddleFiller;
    Object floor_RightFiller;

    Object water;
    Object platform_left;
    Object platform_filler;
    Object platform_right;

    Object player;
    Object enemy;
    Object movingPlatform;
    Object ladder;

    Object door;

    string filename;
    float offset = 2.55f;
    GameObject groundContainer;

    List<Object> blankList = new List<Object>();

    public const string stree_0 = "t";
    public const string stree_1 = "t2";
    public const string srock = "r";
    public const string sfloor_Topleft = "1";
    public const string sfloor_TopFiller = "2";
    public const string sfloor_TopRight = "3";

    public const string sfloor_LeftFiller = "7";
    public const string sfloor_MiddleFiller = "8";
    public const string sfloor_RightFiller = "9";

    public const string swater = "w";
    public const string sblank = "b";
    public const string sdoor = "d";

    public const string splatform_left = "4";
    public const string splatform_filler = "5";
    public const string splatform_right = "6";


    public const string splayer = "p";
    public const string senemy = "e";
    public const string smovingPlatform = "m";
    public const string sladder = "l";


    [MenuItem("Window/LevelCreation")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(CreateLevel));
    }

    private void OnGUI()
    {
        GUILayout.Label("Level Generator", EditorStyles.boldLabel);

        tree_0 = EditorGUILayout.ObjectField("tree_0", tree_0, typeof(GameObject), true);
        tree_1 = EditorGUILayout.ObjectField("tree_1", tree_1, typeof(GameObject), true);
        rock = EditorGUILayout.ObjectField("rock", rock, typeof(GameObject), true);

        floor_TopLeft = EditorGUILayout.ObjectField("floor_TopLeft", floor_TopLeft, typeof(GameObject), true);
        floor_TopFiller = EditorGUILayout.ObjectField("floor_TopFiller", floor_TopFiller, typeof(GameObject), true);
        floor_TopRight = EditorGUILayout.ObjectField("floor_TopRight", floor_TopRight, typeof(GameObject), true);

        floor_LeftFiller = EditorGUILayout.ObjectField("floor_LeftFiller", floor_LeftFiller, typeof(GameObject), true);
        floor_MiddleFiller = EditorGUILayout.ObjectField("floor_MiddleFiller", floor_MiddleFiller, typeof(GameObject), true);
        floor_RightFiller = EditorGUILayout.ObjectField("floor_RightFiller", floor_RightFiller, typeof(GameObject), true);


        water = EditorGUILayout.ObjectField("water", water, typeof(GameObject), true);
        platform_left = EditorGUILayout.ObjectField("platform_left", platform_left, typeof(GameObject), true);
        platform_filler = EditorGUILayout.ObjectField("platform_filler", platform_filler, typeof(GameObject), true);
        platform_right = EditorGUILayout.ObjectField("platform_right", platform_right, typeof(GameObject), true);

        player = EditorGUILayout.ObjectField("Player", player, typeof(GameObject), true);
        enemy = EditorGUILayout.ObjectField("Enemy", enemy, typeof(GameObject), true);
        movingPlatform = EditorGUILayout.ObjectField("MovingPlatform", movingPlatform, typeof(GameObject), true);
        ladder = EditorGUILayout.ObjectField("Ladder", ladder, typeof(GameObject), true);
        door = EditorGUILayout.ObjectField("Door", door, typeof(GameObject), true);
        filename = EditorGUILayout.TextField("Level File", filename);
        if (GUILayout.Button ("Generate"))
        {
            GenerateLevel();
        }

    }

    void GenerateLevel()
    {
        string[][] jagged = readFile(Application.dataPath + "/LevelFiles/" + filename + ".txt");

        for (int y = 0; y < jagged.Length; y++)
        {
            for (int x = 0; x < jagged[y].Length; x++)
            {
                switch (jagged[y][x])
                {
                    case stree_0:
                        Instantiate(tree_0, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        Instantiate(floor_TopFiller, new Vector2(x * offset, y * -offset + 6.8f), Quaternion.identity);
                        break;
                    case stree_1:
                        Instantiate(tree_1, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case srock:
                        Instantiate(rock, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sfloor_Topleft:
                        Instantiate(floor_TopLeft, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sfloor_TopFiller:
                        Instantiate(floor_TopFiller, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sfloor_TopRight:
                        Instantiate(floor_TopRight, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;

                    case sfloor_LeftFiller:
                        Instantiate(floor_LeftFiller, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sfloor_MiddleFiller:
                        Instantiate(floor_MiddleFiller, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sfloor_RightFiller:
                        Instantiate(floor_RightFiller, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                        
                    case swater:
                        Instantiate(water, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case splatform_left:
                        Instantiate(platform_left, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case splatform_filler:
                        Instantiate(platform_filler, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case splatform_right:
                        Instantiate(platform_right, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sblank:
                        Object o = Instantiate(player, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        blankList.Add(o);
                        break;

                    case splayer:
                        Instantiate(player, new Vector2(x * offset, y * -offset + 1.5f), Quaternion.identity);
                        break;
                    case senemy:
                        Instantiate(enemy, new Vector2(x * offset, y * -offset + 1.5f), Quaternion.identity);
                        break;
                    case smovingPlatform:
                        Instantiate(movingPlatform, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sladder:
                        Instantiate(ladder, new Vector2(x * offset, y * -offset + 4f), Quaternion.identity);
                        Instantiate(floor_TopFiller, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    case sdoor:
                        Instantiate(door, new Vector2(x * offset, y * -offset), Quaternion.identity);
                        break;
                    default:
                        break;
                }
            }
        }
        for (int i = 0; i < blankList.Count; i++)
        {
            DestroyImmediate(blankList[i]);
        }
    }

    string[][] readFile(string file)
    {
        string text = System.IO.File.ReadAllText(file);
        string[] lines = Regex.Split(text, "\n");
        int rows = lines.Length;
        string[][] levelBase = new string[rows][];
        for (int i = 0; i < lines.Length; i++)
        {
            string[] stringOfLine = Regex.Split(lines[i], "");
            levelBase[i] = stringOfLine;
        }
        return levelBase;
    }

}
