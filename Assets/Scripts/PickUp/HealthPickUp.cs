﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : PickUp {


    [SerializeField]
    private int healthPickUp;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip healthPickUpSound;
    [SerializeField] private SpriteRenderer spriteRenderer;

    private Rigidbody2D rigidbody2D;

    public override void OnCollision()
    {
        Player.Instance.HealthStat.CurrentValue += healthPickUp;
        spriteRenderer.enabled = false;
        audioSource.PlayOneShot(healthPickUpSound);
        Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
        StartCoroutine(waitToDestroy(healthPickUpSound.length));
    }

    IEnumerator waitToDestroy(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }

    // Use this for initialization
    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), true);
    }

    // Update is called once per frame
    void Update () {
		
	}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {

            rigidbody2D.gravityScale = 0f;
            rigidbody2D.isKinematic = true;
            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);

        }
    }


}
