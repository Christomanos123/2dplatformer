﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : PickUp {

    private Rigidbody2D rigidbody2D;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip coinPickUp;
    [SerializeField] private SpriteRenderer spriteRenderer;


    public override void OnCollision()
    {
        GameManager.Instance.CollectedCoins++;
        audioSource.PlayOneShot(coinPickUp,0.5f);
        spriteRenderer.enabled = false;
        Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
        StartCoroutine(waitToDestroy(coinPickUp.length));
    }
    IEnumerator waitToDestroy(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }
    // Use this for initialization
    void Awake () {
        //audioSource.clip = coinPickUp;
        
        rigidbody2D = GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), true);
    }
	
	// Update is called once per frame
	void Update ()
    {
       
	}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            
            rigidbody2D.gravityScale = 0f;
            rigidbody2D.isKinematic = true;
            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
            
        }
    }

   

}
