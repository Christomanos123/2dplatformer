﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ActivateBossFight : MonoBehaviour {

    //Disable/Enable exit, Enable boss's health bar, play boss battle music,set the target

    [SerializeField] private Boss boss;
    [SerializeField] private GameObject exit;
    [SerializeField] private GameObject target;
    [SerializeField] private Slider bossHealthSlider;

    private bool bossBattleEnded;
    public bool BossBattleEnded
    {
        get
        {
            return bossBattleEnded;
        }

        set
        {
            bossBattleEnded = value;
        }
    }

    // Use this for initialization
    void Start () {

        exit.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (BossBattleEnded)
        {
            exit.gameObject.SetActive(true);
        }
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            boss.Target = target;
            bossHealthSlider.gameObject.SetActive(true);
        }
    }
}
