﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private bool dataSaved;
    Canvas messageCanvas;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip checkPointSound;


    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        messageCanvas = GetComponentInChildren<Canvas>();
        dataSaved = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (!dataSaved)
            {
                Debug.Log("Checkpoint reached");
                audioSource.PlayOneShot(checkPointSound,0.4f);
                GameManager.Instance.Save();
                dataSaved = true;
            }
                Use();
                spriteRenderer.color = new Color(0, 0.0349293f, 0.6431373f, 0.5490196f);

           

        }
    }

    private void Use()
    {
        messageCanvas.enabled = true;
    }
    
}
