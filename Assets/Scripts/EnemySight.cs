﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour {
    [SerializeField]
    private Enemy enemy;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip detectionSound;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(detectionSound, 0.5f);
            enemy.Target = other.gameObject;
        }
    }

  


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
           
           enemy.Target = null;   
        }
    }
}
