﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ButtonOptions : MonoBehaviour {

    private int scene;

    [SerializeField]
    private GameObject continueLockedButton;

    void Start()
    {
        scene = GameManager.Instance.LoadScene;
        Debug.Log("Start scene: " + scene);
    }

    private void Update()
    {
        LockContinueButton();
    }

    public void NewGame()
    {
        GameManager.Instance.Health = 100;
        GameManager.Instance.CollectedCoins = 0;
        GameManager.Instance.NumberOfLives = 3;

        if (File.Exists(Application.dataPath + "/Save/data.json"))
        {
            File.Delete(Application.dataPath + "/Save/data.json");
        }
        SceneManager.LoadScene(1);
    }


    public void Continue()
    {
       Debug.Log("Continue scene: " + scene);
       SceneManager.LoadScene(scene);
    }

    public void LockContinueButton()
    {
        if (File.Exists(Application.dataPath + "/Save/data.json"))
        {
            continueLockedButton.SetActive(false);
            //Destroy(continueLockedButton.gameObject);
        }
    }


    //Level Select
    public void LevelSelect()
    {
        SceneManager.LoadScene(2);
    }

    


    public void QuitGame()
    {
        #if UNITY_EDITOR
            EditorApplication.isPlaying = false;
        #else    
            Application.Quit();
        #endif
    }

}
