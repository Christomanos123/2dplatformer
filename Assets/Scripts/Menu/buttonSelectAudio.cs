﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class buttonSelectAudio : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip hoverButtonSound;
    [SerializeField] AudioClip clickButtonSound;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        audioSource.PlayOneShot(hoverButtonSound);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.PlayOneShot(clickButtonSound);
    }

}
