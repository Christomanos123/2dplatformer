﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour
{


    Canvas m_canvas;

    [SerializeField]
    GameObject exitPrompt;

    [SerializeField] AudioSource audioSource;

    // Use this for initialization
    void Start () {
        m_canvas = GetComponentInParent<Canvas>();
        m_canvas.enabled = false;
        Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseButtonPressed();
        }
	}

    


   public void PauseButtonPressed()
    {
        if (m_canvas.isActiveAndEnabled)
        {
            m_canvas.enabled = false;
            audioSource.volume = 0.1f;
        }
        else
        {
            audioSource.volume = 0.03f;
            m_canvas.enabled = true;
        }

        Pause();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Pause()
    {
        //Pause the game
        Time.timeScale = (Time.timeScale == 0) ? 1 : 0;
    }

    public void Quit()
    {
        #if UNITY_EDITOR
        EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    public void ExtiToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitPrompt()
    {
        exitPrompt.SetActive(true);
    }

    public void Cancel()
    {
        exitPrompt.SetActive(false);
    }

 
}
