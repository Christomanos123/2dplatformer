﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }

    }
    public Player player;

    [SerializeField]
    private GameObject coinPrefab;

    [SerializeField]
    private GameObject healthPrefab;

    [SerializeField]
    private Text coinText;

    [SerializeField]
    private Text livesText;

    


    private static int numberOfLives;
    public int NumberOfLives
    {
        get
        {
            return numberOfLives;
        }

        set
        {
            if (livesText != null)
            {
                livesText.text = value.ToString();
            }
            numberOfLives = value;
        }
    }


    public GameObject CoinPrefab
    {
        get
        {
            return coinPrefab;
        }
    }

    private static int collectedCoins;
    public int CollectedCoins
    {
        get
        {
            return collectedCoins;
        }

        set
        {
            if (coinText != null)
            {
                coinText.text = value.ToString();
            }
            collectedCoins = value;
        }
    }

    public GameObject HealthPrefab
    {
        get
        {
            return healthPrefab;
        }

        set
        {
            healthPrefab = value;
        }
    }


   
    private static int loadScene;
    public int LoadScene
    {
        get
        {
            return loadScene;
        }

        set
        {
            loadScene = value;
        }
    }
    private static float health;
    public float Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
        }
    }


    private static Vector2 playerPos;
    public Vector2 PlayerPos
    {
        get
        {
            return playerPos;
        }

        set
        {
            playerPos = value;
        }
    }


    // Use this for initialization
    void Start () {
        //Load();
        Health = 100;
	}
	
	// Update is called once per frame
	void Update () {

        //Load
        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }

        //Save
        if (Input.GetKeyDown(KeyCode.K))
        {
            Save();
        }

    }

    public void Save()
    {
        health = player.HealthStat.CurrentValue;
        Debug.Log("Data Saved");
        JsonSaver.Instance.SaveGameData();
    }

    public void Load()
    {
        if (File.Exists(Application.dataPath + "/Save/data.json"))
        {
            JsonSaver.Instance.LoadGameData();
            if (player != null)
            {
                player.transform.position = playerPos;
                player.HealthStat.CurrentValue = health;
            }
            Debug.Log("Data loaded");
        }
    }

     void OnLevelFinishLoading(Scene scene, LoadSceneMode mode)
    {
        if (health != 0)
        {
            player.HealthStat.CurrentValue = health;
        }

        if (coinText != null)
        {
            coinText.text = CollectedCoins.ToString();
        }

        if (livesText != null)
        {
            livesText.text = NumberOfLives.ToString();
        }

    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishLoading;
    }

}
