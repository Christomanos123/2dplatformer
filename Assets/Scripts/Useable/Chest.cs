﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour, IUseable
{
    public Animator ChestAnimator;

    enum ChestTypes {Coins,Health }

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip openChestSound;
   
    private int amountToSpawn;

    private GameObject objectToSpawn;

    private bool isOpen;

    [SerializeField]
    private ChestTypes chestTypes;

    private GameManager gameManager;

    public void Use()
    {
        audioSource.PlayOneShot(openChestSound);
        UseChest("open");
    }




    // Use this for initialization
    void Start () {
        gameManager = GameManager.Instance;
        switch (chestTypes)
        {
            case ChestTypes.Health:
                objectToSpawn = gameManager.HealthPrefab;
                amountToSpawn = Random.Range(1, 2);
                break;
                case ChestTypes.Coins:
                objectToSpawn = gameManager.CoinPrefab;
                amountToSpawn = Random.Range(1, 5);
                break;
            
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void UseChest(string trigger)
    {
        if (!isOpen)
        {
            ChestAnimator.SetTrigger(trigger);
            isOpen = true;
            for (int i = 0; i < amountToSpawn; i++)
            {
                GameObject coin = (GameObject)Instantiate(objectToSpawn, 
                new Vector3(transform.position.x, transform.position.y), Quaternion.identity);
            }
        }

    }

}
