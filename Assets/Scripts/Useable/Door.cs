﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour,IUseable
{

  [SerializeField]  private AudioSource audioSource;
  [SerializeField] private AudioClip m_OpenWoodenDoorSound;

    public void Use()
    {
        audioSource.PlayOneShot(m_OpenWoodenDoorSound);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Debug.Log("door used");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
