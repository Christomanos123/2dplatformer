﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSign : MonoBehaviour
{
    [SerializeField]
    Canvas messageCanvas;

    [SerializeField] AudioSource popSFX;

    // Use this for initialization
    void Start () {
        messageCanvas.enabled = false;	
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            popSFX.Play();
            Use();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            TurnOffMessage();
        }
    }


    private void Use()
    {
        messageCanvas.enabled = true;
    }

    private void TurnOffMessage()
    {
        messageCanvas.enabled = false;
    }


}
