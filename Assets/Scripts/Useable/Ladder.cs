﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour, IUseable
{
    [SerializeField]
    private Collider2D platformCollider;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Use()
    {
        if (Player.Instance.OnLadder)
        {
            //We need to stop climbing
            UseLadder(false,1,0,1,"land");
        }
        else
            //We need to start climbing
            UseLadder(true,0,1,0,"reset");
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), platformCollider, true);

    }

    private void UseLadder(bool onLadder,int gravityScale, int layerWeight, int animSpeed, string trigger)
    {
        Player.Instance.OnLadder = onLadder;
        Player.Instance.MyRigidbody2D.gravityScale = gravityScale;
        Player.Instance.MyAnimator.SetLayerWeight(2, layerWeight);
        Player.Instance.MyAnimator.speed = animSpeed;
        Player.Instance.MyAnimator.SetTrigger(trigger);

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            UseLadder(false, 1,0,1,"land");
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), platformCollider, false);
        }
    }

}
