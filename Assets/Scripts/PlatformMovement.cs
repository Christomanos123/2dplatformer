﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    //Speed, posA,posB, nextPos ,childTranform, tranformPosb

    [SerializeField]
    private float speed;

    
    private Vector3 posA;
    private Vector3 nextPos;
    private Vector3 posB;

    
    private float waitTimer;
    private float waitCoolDown;


    [SerializeField]
    private Transform childTranformPos;

    [SerializeField]
    private Transform transformPosB;

    private void Start()
    {
        waitCoolDown = UnityEngine.Random.Range(0,5);
        posA = childTranformPos.localPosition;
        posB = transformPosB.localPosition;
        nextPos = posB;
    }


    private void Update()
    {
        Move();
    }

    private void Move()
    {
        childTranformPos.localPosition = Vector3.MoveTowards(childTranformPos.localPosition, nextPos, speed * Time.deltaTime);

        if (Vector3.Distance(childTranformPos.localPosition,nextPos) <= 0.1)
        {
            waitTimer += Time.deltaTime;
            if (waitTimer >= waitCoolDown)
            {
                ChangeDestination();
                waitTimer = 0;
            }
        }
    }

    private void ChangeDestination()
    {
        nextPos = nextPos != posA ? posA : posB;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.SetParent(childTranformPos);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.SetParent(null);
        }
    }

}
