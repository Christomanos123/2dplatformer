﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BossTest : MonoBehaviour {

    private BossActionType eCurrentState = BossActionType.Idle;

    [SerializeField] private float health;
    [SerializeField] private Slider healthSlider;
    private float idleTimer;
    private float idleDuration;

    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;

    bool facingRight = true;
    bool jump = false;
    private bool directionRight = true;

    public enum BossActionType
    {
        Idle,
        Chase,
        Attacking,

    }



    private Animator animator;

    private bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }




    //Health, Movement speed, Phase 1 and 2, Throw Fireballs, jump and run, Detect the player



    // Use this for initialization
    public void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        healthSlider.value = health;
        UpdateStates();
    }

    //Detect Player


    //Take Damage
    private void OnTriggerEnter2D(Collider2D collision)
    {

    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    void UpdateStates()
    {
        switch (eCurrentState)
        {
            case BossActionType.Idle:
                {
                    HandleIdleState();
                    break;
                }
        }
    }

    void HandleIdleState()
    {
        animator.SetFloat("speed", 0);
        idleTimer += Time.deltaTime;

        if (idleTimer >= idleDuration)
        {
            eCurrentState = BossActionType.Chase;
        }
    }

    void HandleChaseState()
    {

    }
}
