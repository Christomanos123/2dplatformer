﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour {

    //Health, Movement speed, Phase 1 and 2, Throw Fireballs, jump and run,target = player
    //Phase 1: Melee, Melee, Jump back, wait 5 seconds, repeat.
    //Phase 2: Shoot 3 times, melee 2 times, jump once.
    //Phase 3: Idle and jump and spikes.

    [SerializeField] public BossActionType eCurrentState = BossActionType.Idle;

    public enum BossActionType
    {
        Idle,
        Chase,
        Decision,
        Attack,
        Dead,
    }

   [SerializeField] private BossAttackPhase eCurrentAttackPhase = BossAttackPhase.Melee;
    public enum BossAttackPhase
    {
        Melee,
        JumpBackwards,
        Shoot,
    }

    [SerializeField]  public float g_CoolDownDuration;
    public float g_CoolDownDurationAddition;
    private float m_CoolDownTimer;

    private int[] m_Pattern1 = { 2, 0, 1 };
    private int[] m_Pattern2 = { 2, 0, 1 };
    private int[] m_Pattern3 = { 2, 2, 1 };
    private int[] m_CurrentPattern;
    [SerializeField] private int m_PatternCounter;

    private Animator m_Animator;
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody2D;
    [SerializeField] private List<string> m_DamageSources;

    [SerializeField] private ActivateBossFight m_ActivateBossFight;
    [SerializeField] private float m_Health;
    [SerializeField] private Slider m_HealthSlider;

    [SerializeField] private bool m_DropItem = true;
    [SerializeField] private float m_AttackRange;
    [SerializeField] private float m_CurrentRange;
    //[SerializeField] private float throwRange;

    [SerializeField] private Transform m_LeftEdge;
    [SerializeField] private Transform m_RightEdge;
    
    [SerializeField] private float m_MeleeCoolDown;
     private float m_MeleeTimer;

    private float m_ShootTimer;
    [SerializeField] private float m_ShootCoolDown;
    private bool m_CanShoot = true;


    [SerializeField] private float m_JumpForce;
    bool m_Jump = false;

    [SerializeField] private float m_MovementSpeed;
    [SerializeField] bool m_IsFacingRight;

    public GameObject Target { get; set; }
   [SerializeField] public bool Attack { get; set; }


    private int m_CurrentPatternAttackType;

    [SerializeField] private bool m_CanMelee = true;
    [SerializeField] private bool m_HasAttacked = false;
    [SerializeField] private bool m_OnCoolDown = false;
    

    [SerializeField] private GameObject m_FireBallPrefab;
    [SerializeField] private Transform m_FireBallPos;
    [SerializeField] private EdgeCollider2D m_SwordCollider;


    public bool InAttackRange
    {
        get
        {
            if (Target != null)
            {
                m_CurrentRange = Vector2.Distance(transform.position, Target.transform.position);
                return m_CurrentRange <= m_AttackRange;
            }

            return false;
        }
    }

    private  bool IsDead
    {
        get
        {
            return m_Health <= 0;
        }
    }
    
  
    // Use this for initialization
    public  void Start () {
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        m_IsFacingRight = true;
        m_PatternCounter = 0;
        Player.Instance.DeadEvent += new DeadHandlerEvent(RemoveTarget);
    }
	
	// Update is called once per frame
	void Update () {
        m_HealthSlider.value = m_Health;
        if (!IsDead)
        {
            UpdateStates();
        }
	}


    //Take Damage
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (m_DamageSources.Contains(collision.tag))
        {
            TakeDamage();
        }
    }

    void RemoveTarget()
    {
         Target = null;
         eCurrentState = BossActionType.Idle;
    }

    void TakeDamage()
    {
        if (!IsDead)
        {
            m_Health -= 10;
        }
        else
        {
            if (m_DropItem)
            {

                //Coin spawn
                Debug.Log("Coin");
                for (int i = 1; i <= 10; i++)
                {
                    GameObject coin = (GameObject)Instantiate(GameManager.Instance.CoinPrefab, new Vector3(transform.position.x + 1, transform.position.y), Quaternion.identity);
                    Physics2D.IgnoreCollision(coin.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                }
                 m_DropItem = false; 
            }
            //Death
            Target = null;
            m_ActivateBossFight.BossBattleEnded = true;
            m_Animator.SetTrigger("die");
            //PlayAudioOnce(deathSound, 0.4f);
        }
    }

   public void ChangeDirection()
    {
        m_IsFacingRight = !m_IsFacingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    void UpdateStates()
    {
        if (Target != null) {
            if (!InAttackRange && !Attack && m_OnCoolDown == false)
            {
                eCurrentState = BossActionType.Chase;
            }
            else if(InAttackRange && !Attack)
            {
                eCurrentState = BossActionType.Decision;
            }
        }

        switch(eCurrentState)
        {
            case BossActionType.Idle:
                {
                    HandleIdleState();
                    break;
                }
            case BossActionType.Chase:
                {
                    HandleChaseState();
                    break;
                }
            case BossActionType.Decision:
                {
                    HandleDecisionState();
                    break;
                }
            case BossActionType.Attack:
                {
                    Debug.Log("inAttackType");

                    m_CurrentPatternAttackType = m_CurrentPattern[m_PatternCounter];
                    eCurrentAttackPhase = (BossAttackPhase) m_CurrentPatternAttackType;
                    HandleAttackState();
                    m_PatternCounter++;
                    if (m_PatternCounter == m_CurrentPattern.Length)
                    {
                        m_PatternCounter = 0;
                    }
                    break;
                }
        }
    }


    void HandleDecisionState()
    {
        Debug.Log("HandleDecisionState");
        m_Rigidbody2D.velocity = Vector3.zero;
        m_Animator.SetFloat("speed", 0);
        
        if (m_HasAttacked == false)
        {
            Debug.Log("attackisfalse");
            if (!InAttackRange && !Attack && m_OnCoolDown == false)
            {
                Debug.Log("gotoChase");
                eCurrentState = BossActionType.Chase;
            }

            
            if (m_Health > 100 && m_Health <= 150)
            {
                g_CoolDownDurationAddition = 2;
                m_CurrentPattern = m_Pattern1;
                Debug.Log("Phase1");
                Debug.Log("Type1 " + m_CurrentPatternAttackType);
                Attack = true;
                eCurrentState = BossActionType.Attack;
                m_HasAttacked = true;
            }
            else if (m_Health <= 100 && m_Health > 50)
            {
                Debug.Log("Phase2");
                g_CoolDownDurationAddition = 1.5f;
                m_CurrentPattern = m_Pattern2;
                Debug.Log("Type2 " + m_CurrentPatternAttackType);
                Attack = true;
                eCurrentState = BossActionType.Attack;
                m_HasAttacked = true;
            }
            else if (m_Health <= 50 && m_Health > 0)
            {
                Debug.Log("Phase3");
                g_CoolDownDurationAddition = 0f;
                m_CurrentPattern = m_Pattern3;
                Debug.Log("Type3 " + m_CurrentPatternAttackType);
                Attack = true;
                eCurrentState = BossActionType.Attack;
                m_HasAttacked = true;
            }
          
        }


        CoolDownTimer();

    }
    
    void CoolDownTimer()
    {
        if (m_OnCoolDown)
        {
            m_CoolDownTimer += Time.deltaTime;
            if (m_CoolDownTimer >= g_CoolDownDuration)
            {
                m_OnCoolDown = false;
                m_HasAttacked = false;
                m_CanMelee = true;
                m_CanShoot = true;
                m_CoolDownTimer = 0;
            }
        }
    }


    void HandleIdleState()
    {
        m_Animator.SetFloat("speed", 0);

        //idleTimer += Time.deltaTime;

        //if (idleTimer >= idleDuration)
        //{
        //    eCurrentState = BossActionType.Chase;
        //}
    }

    void HandleChaseState()
    {
        if (m_Rigidbody2D.velocity.y == 0)
        {
            //Do not pass the edges
            if ((GetDirection().x < 0 && transform.position.x > m_LeftEdge.position.x) || (GetDirection().x > 0 && transform.position.x < m_RightEdge.position.x))
            {
                m_Animator.SetFloat("speed", 1);
                transform.Translate(GetDirection() * (m_MovementSpeed * Time.deltaTime));
            }
            else
             ChangeDirection();

            LookAtTarget();
           
        }
    }

    void HandleDeathState()
    {

    }

   
    void HandleAttackState()
    {
        Debug.Log("AttackState");
       switch(eCurrentAttackPhase)
        {
            case BossAttackPhase.Melee:
                Melee();
                break;
            case BossAttackPhase.JumpBackwards:
                Debug.Log("JUMPING");
                JumpBackwards();
                break;
            case BossAttackPhase.Shoot:
                Shoot();
                break;
            
        }
    }

    void Melee()
    {
        Debug.Log("OutsideMelee");
        if (m_CanMelee)
        {
            Debug.Log("CanMelee");
            LookAtTarget();
            m_Animator.SetTrigger("melee");
            m_CanMelee = false;
            m_OnCoolDown = true;
            eCurrentState = BossActionType.Decision;          
        }
    }

    public void EnableSwordCollider()
    {
       m_SwordCollider.enabled = true;
    }

    public void DisableSwordCollider()
    {
       m_SwordCollider.enabled = false;
    }

    private void Shoot()
    {        
        if (m_CanShoot)
        {
            LookAtTarget();
            m_Animator.SetTrigger("shoot");
            m_CanShoot = false;
            m_OnCoolDown = true;
            eCurrentState = BossActionType.Decision;
        }
    }

    public void ShootFireBall()
    {
        if (m_IsFacingRight)
        {
            GameObject tmp = (GameObject)Instantiate(m_FireBallPrefab, m_FireBallPos.position, Quaternion.Euler(0, 0, 180));
            tmp.GetComponent<Fireball>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(m_FireBallPrefab, m_FireBallPos.position, Quaternion.Euler(0, 0, 0));
            tmp.GetComponent<Fireball>().Initialize(Vector2.left);
        }
    }


    void JumpBackwards()
    {
            m_Animator.SetTrigger("jump");
            GetComponent<ParabolaController>().FollowParabola();
            m_OnCoolDown = true;
            eCurrentState = BossActionType.Decision;        
    }

    public Vector2 GetDirection()
    {
        return m_IsFacingRight? Vector2.right : Vector2.left;
    }


    private void LookAtTarget()
    {
        if (Target != null)
        {
            float xDir = Target.transform.position.x - transform.position.x;

            if (xDir < 0 && m_IsFacingRight || xDir > 0 && !m_IsFacingRight)
            {
                ChangeDirection();
            }
        }
    }

}
