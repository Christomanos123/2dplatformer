﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPercentage : MonoBehaviour {

    private static LevelPercentage instance;
    public static LevelPercentage Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LevelPercentage>();
            }
            return instance;
        }

    }

    [SerializeField] private Transform startPos;
    public Transform StartPos
    {
        get
        {
            return startPos;
        }
    }

    [SerializeField] private Transform endPos;
    public Transform EndPos
    {
        get
        {
            return endPos;
        }
    }



    [SerializeField] private float overall;
    [SerializeField] private float playerPos;
    [SerializeField] private float normalized;
    
    public float Normalized
    {
        get
        {
            return normalized;
        }
    }



    // Use this for initialization
    void Start () {
        overall = Vector2.Distance(StartPos.position, EndPos.position);
	}
	
	// Update is called once per frame
	void Update () {
        
       playerPos  =  Vector2.Distance(StartPos.position, Player.Instance.transform.position);

       normalized = Mathf.Clamp01(playerPos / overall);
        
	}
}
