﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState
{
    private Enemy enemy;
    private float patrolTimer;
    private float patrolDuration;
    private float rotationSpeed = 10f;
    public void Enter(Enemy enemy)
    {
        patrolDuration = UnityEngine.Random.Range(1, 10);
        this.enemy = enemy;
    }

    public void Execute()
    {
        Patrol();
        enemy.Move();
        ChangeToRangedState();
    }

    private void ChangeToRangedState()
    {
        if (enemy.Target != null && enemy.InThrowRange)
        {
            if (enemy.tag == "FlyingEnemy")
            {
               FaceTarget();
            }
            enemy.ChangeState(new RangedState());

        }
    }

    public void Exit()
    {
       
    }

    private void FaceTarget()
    {
        Vector3 playerPos = Player.Instance.transform.position;
        
        Vector3 vectorToTarget = playerPos - enemy.transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, q, Time.deltaTime * rotationSpeed);
    }

    public void OnTriggerEnter(Collider2D other)
    {
        if (other.CompareTag("Knife") || other.CompareTag("Sword"))
        {
            enemy.Target = Player.Instance.gameObject;
        }

    }

    private void Patrol()
    {
        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolDuration)
        {
            enemy.ChangeState(new IdleState());
        }
    }
}
