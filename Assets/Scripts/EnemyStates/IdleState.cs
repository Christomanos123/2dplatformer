﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IEnemyState
{
    private Enemy enemy;
    private float idleTimer;
    private float idleDuration;
    
    public void Enter(Enemy enemy)
    {
        idleDuration = Random.Range(1, 10);
        this.enemy = enemy;
    }

    public void Execute()
    {
        
        Idle();

        if (enemy.Target != null)
        {
            enemy.ChangeState(new PatrolState());
        }
    }

    public void Exit()
    {
       
    }

    public void OnTriggerEnter(Collider2D other)
    {
        //This will acquire the enemy a target even if it is out of sight.
        //If we get hit by a knife, our target is now the player.
        if (other.CompareTag("Knife") || other.CompareTag("Sword"))
        {
            enemy.Target = Player.Instance.gameObject;
        }
    }

    private void Idle()
    {
        //When we are idling: 1) we need to stop moving 2) Kick off the wait timer 3) change to patrol state
        enemy.MyAnimator.SetFloat("speed", 0);
        idleTimer += Time.deltaTime;

        if (idleTimer >= idleDuration)
        {
            enemy.ChangeState(new PatrolState());
        }
    }


}
