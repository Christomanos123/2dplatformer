﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeState : IEnemyState
{
    private Enemy enemy;
    
    private float attackTimer;
    private float attackCoolDown = 2f;
    private bool canAttack = true;

    public void Enter(Enemy enemy)
    {
       this.enemy = enemy;
    }

    public void Execute()
    {
        if (!enemy.TakingDamage)
        {
            Attack();
            //Target is too far away to attack
            if (!enemy.InMeleeRange && enemy.InThrowRange)
            {
                enemy.ChangeState(new RangedState());
            }
            else if (enemy.Target == null)
            {
                enemy.ChangeState(new IdleState());
            }
       }

    }

    public void Exit()
    {
      
    }

    public void OnTriggerEnter(Collider2D other)
    {
       
    }



    private void Attack()
    {
        attackTimer += Time.deltaTime;

        if (attackTimer >= attackCoolDown)
        {
            canAttack = true;
            attackTimer = 0;
        }

        if (canAttack)
        {
            enemy.MyAnimator.ResetTrigger("throw");
            enemy.MyAnimator.SetTrigger("attack");

           enemy.MeleeAttack();

            canAttack = false;
        }
    }

}
