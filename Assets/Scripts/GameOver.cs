﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    [SerializeField] private Image gameOverImage;
    [SerializeField] private Text gameOverText;

    [SerializeField] private Text numOfLivesText;

    [SerializeField] private Image heartImage;
    [SerializeField] private RectTransform HeartTransform;
    [SerializeField] private Sprite heart, brokenHeart;
    [SerializeField] private float flickeringTime;

    [SerializeField] private Slider levelProgressBar;

    [SerializeField] private Image sliderBG;
    [SerializeField] private Image sliderFill;
    [SerializeField] private Image sliderHandle;
    [SerializeField] private Image sliderDoorImage;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip gameOverSound;

    private bool flickering;
    Sequence currentSequence;

	// Use this for initialization
	void Awake () {
        numOfLivesText.text = GameManager.Instance.NumberOfLives.ToString();
    }
	
	// Update is called once per frame
	void Update () {


        //If player is dead then play game over sequence
        if (Player.Instance.IsDead && currentSequence == null)
        {

            Player.Instance.AllowedToMove = false;
            currentSequence = gameOverSequence(2.0f); 
            currentSequence.Play().OnComplete(PlayerRespawn);
        }

        //If player has fallen off a cliff then play the game over sequence.
        else if (Player.Instance.transform.position.y <= -10f && currentSequence == null)
        {
            Player.Instance.AllowedToMove = false;
            currentSequence = gameOverSequenceInstant();
            currentSequence.Play().OnComplete(PlayerRespawn);
            PlayGameOverSound();
        }

    }

    void OnCompletion()
    {
        Debug.Log("on completion");
        GameManager.Instance.NumberOfLives--;
        

        if (GameManager.Instance.NumberOfLives >= 0)
        {
            numOfLivesText.text = GameManager.Instance.NumberOfLives.ToString();
        }

        levelProgressBar.value = Mathf.Lerp(0, 1, LevelPercentage.Instance.Normalized);
    }

    void PlayerRespawn()
    {
        if (GameManager.Instance.NumberOfLives < 0)
        {
            GameManager.Instance.Health = 100;
            GameManager.Instance.CollectedCoins = 0;
            GameManager.Instance.NumberOfLives = 3;
            SceneManager.LoadScene(1);
        }
        Player.Instance.Death();

        currentSequence = gameOverSequenceFadeOut(3.0f);
        currentSequence.Play().OnComplete(ResetVariables);
    }



    private void ResetVariables()
    {
        currentSequence = null;
    }

    void DisableHeart()
    {
        HeartTransform.sizeDelta = new Vector2(1067, 1002);
        heartImage.sprite = brokenHeart;

    }

    void AllowPlayerMovement()
    {
        Player.Instance.AllowedToMove = true;
    }
 
    void PlayGameOverSound()
    {
        audioSource.PlayOneShot(gameOverSound,0.5f);
    }


    Sequence gameOverSequence(float delay)
    {
        Sequence s = DOTween.Sequence();
        s.Append(gameOverImage.DOFade(1, 2f).SetDelay(delay));
        s.Join(heartImage.DOFade(1, 1.5f).OnComplete(DisableHeart));
        s.Join(numOfLivesText.DOFade(1, 1.5f).OnComplete(OnCompletion));
        s.Join(gameOverText.DOFade(1, 1f).OnComplete(PlayGameOverSound));
        s.Join(gameOverText.GetComponent<RectTransform>().DOScale(1, 2f));
        s.Join(sliderBG.DOFade(1, 2f));
        s.Join(sliderFill.DOFade(1, 2f));
        s.Join(sliderHandle.DOFade(1, 2f));
        s.Join(sliderDoorImage.DOFade(1, 2f));
        return s;
    }

    Sequence gameOverSequenceInstant()
    {
        Sequence s = DOTween.Sequence();
        s.Append(gameOverImage.DOFade(1, 2f));
        s.Join(heartImage.DOFade(1, 1.5f).OnComplete(DisableHeart));
        s.Join(numOfLivesText.DOFade(1, 1.5f).OnComplete(OnCompletion));
        s.Join(gameOverText.DOFade(1, 1f)).OnComplete(PlayGameOverSound);
        s.Join(gameOverText.GetComponent<RectTransform>().DOScale(1, 2f));
        s.Join(sliderBG.DOFade(1, 2f));
        s.Join(sliderFill.DOFade(1, 2f));
        s.Join(sliderHandle.DOFade(1, 2f));
        s.Join(sliderDoorImage.DOFade(1, 2f));
        return s;
    }

    Sequence gameOverSequenceFadeOut(float delay)
    {
        Sequence s = DOTween.Sequence();
        s.Append(gameOverImage.DOFade(0, 2f).SetDelay(delay));
        s.Join(heartImage.DOFade(0, 1f).OnComplete(DisableHeart));
        s.Join(numOfLivesText.DOFade(0, 1f));
        s.Join(gameOverText.DOFade(0, 1f).OnComplete(AllowPlayerMovement));
        s.Join(gameOverText.GetComponent<RectTransform>().DOScale(0, 1f));
        s.Join(sliderBG.DOFade(0, 2f));
        s.Join(sliderFill.DOFade(0, 2f));
        s.Join(sliderHandle.DOFade(0, 2f));
        s.Join(sliderDoorImage.DOFade(0, 2f));
        return s;
    }

}
