﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class CameraBoundaries
{
    public float xMin, xMax, yMin, yMax;
}


public class CameraFollow : MonoBehaviour {

    public CameraBoundaries boundary;
    private Transform target;
	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = new Vector3(Mathf.Clamp(target.position.x, boundary.xMin, boundary.xMax),
                            Mathf.Clamp(target.position.y, boundary.yMin, boundary.yMax), transform.position.z);
	}
}
