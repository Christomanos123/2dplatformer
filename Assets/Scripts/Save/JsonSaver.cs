﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class JsonSaver : MonoBehaviour {

    private static JsonSaver instance;

    public static JsonSaver Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<JsonSaver>();
            }
            return instance;
        }

      
    }

    public void SaveGameData()
    {
        GameData data = new GameData();

        data.health = GameManager.Instance.Health;
        data.lives = GameManager.Instance.NumberOfLives;
        data.coins = GameManager.Instance.CollectedCoins;
        data.scene = SceneManager.GetActiveScene().buildIndex;
       data.playerPos = GameManager.Instance.player.transform.position;

        string dataString = JsonUtility.ToJson(data,true);
        if (!Directory.Exists(Application.dataPath + "/Save"))
        {
            Directory.CreateDirectory(Application.dataPath + "/Save");
        }

        File.WriteAllText(Application.dataPath + "/Save/data.json", dataString);
        Debug.Log( "Scene saved = " + data.scene);
        Debug.Log("File created");
    }


    public void LoadGameData()
    {
        GameData data = new GameData();

        string dataString = File.ReadAllText(Application.dataPath + "/Save/data.json");

        data = JsonUtility.FromJson<GameData>(dataString);

        GameManager.Instance.Health = data.health;
        GameManager.Instance.NumberOfLives = data.lives;
        GameManager.Instance.CollectedCoins = data.coins;
        GameManager.Instance.LoadScene = data.scene;
        GameManager.Instance.PlayerPos = data.playerPos;
    }
}
