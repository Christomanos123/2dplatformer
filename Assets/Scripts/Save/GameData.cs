﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class GameData
{
    public float health;
    public int coins;
    public int lives;
    public Vector2 playerPos;


    //Save scene
    public int scene;

}
