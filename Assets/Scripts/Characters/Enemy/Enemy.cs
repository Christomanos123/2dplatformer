﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateStuff;

public class Enemy : Character
{
    

    private IEnemyState currentState;

    public GameObject Target { get; set; }

    [SerializeField] private float meleeRange;
    [SerializeField] private float throwRange;

    [SerializeField] private Transform leftEdge;
    [SerializeField] private Transform rightEdge;

    //Audio Effects
    [SerializeField] AudioSource audioSource;

    //Audio clip one shot
    [SerializeField] AudioClip throwSound;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip deathSound;
    [SerializeField] AudioClip step2Sound;
    [SerializeField] AudioClip damageSound;

    [SerializeField] AudioClip ravenCrySound;

    private Canvas healthCanvas;

    private bool dropItem = true;

    //This property will return true if the distance between the enemy 
    //and the target is less than the meleeRange 
    //else the target is not in the melee range at the momment.
    public bool InMeleeRange
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= meleeRange;
            }

            return false;
        }
    }

    public bool InThrowRange
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= throwRange;
            }

            return false;
        }
    }

    private Vector2 startPos;

    public override void Start ()
    {
        base.Start();
        startPos = transform.position;
        Player.Instance.DeadEvent += new DeadHandlerEvent(RemoveTarget);
        healthCanvas = GetComponentInChildren<Canvas>();
        
        ChangeState(new IdleState());
        if (this.tag == "FlyingEnemy")
        {
            StartCoroutine(PlayCrySound());
        }

    }
	
	public virtual void Update ()
    {
        if (!IsDead)
        {
            if (!TakingDamage)
            {
                //The current state needs be executed and we call it from here.
               currentState.Execute();
            }
            LookAtTarget();
          

        }
    }

    public void RemoveTarget()
    {
        Target = null;
        ChangeState(new PatrolState());
    }

    private void LookAtTarget()
    {
        if (Target != null)
        {
            float xDir = Target.transform.position.x - transform.position.x;

            if (xDir < 0 && facingRight || xDir > 0 && !facingRight)
            {
                ChangeDirection();
            }
        }
    }

    public void ChangeState(IEnemyState newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this);
    }

    public void Move()
    {
        if (!Attack)
        {
            //Do not pass the edges
            if ((GetDirection().x < 0 && transform.position.x > leftEdge.position.x) || (GetDirection().x > 0 && transform.position.x < rightEdge.position.x))
            {
                MyAnimator.SetFloat("speed", 1);
                transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));
            }
            //If you get to the edges change direction
            else if (currentState is PatrolState)
            {
                ChangeDirection();
            }
            else if (currentState is RangedState)
            {
                Target = null;
                ChangeState(new IdleState());
            }
        }
    }
    
    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }


    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        currentState.OnTriggerEnter(other);
      
    }

    //True if enemy is dead, false if alive
    public override bool IsDead
    {
        get
        {
            return healthStat.CurrentValue <= 0;
        }
       
    }

    public override IEnumerator TakeDamage()
    {
        Debug.Log("Taking damage coroutine");

        if (!healthCanvas.isActiveAndEnabled)
        {
            healthCanvas.enabled = true;
        }

        healthStat.CurrentValue -= 10;

        if (!IsDead && !Attack)
        {
            MyAnimator.SetTrigger("damage");
        }
        else if (!IsDead && Attack)
        {
            MyAnimator.SetTrigger("attack");
        }
        else
        {
            if (dropItem)
            {
                //Coin spawn
                GameObject coin = (GameObject)Instantiate(GameManager.Instance.CoinPrefab, new Vector3(transform.position.x, transform.position.y), Quaternion.identity);
                Physics2D.IgnoreCollision(coin.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                dropItem = false;
            }
            //Death
            MyAnimator.SetTrigger("die");
            PlayAudioOnce(deathSound,0.4f);
            yield return null;
        }
        TakingDamage = false;
    }

    public override void Death()
    {
        Destroy(gameObject);
    }

    public override void ChangeDirection()
    {
        Transform tmp = transform.Find("EnemyHealthBarCanvas").transform;
        Vector3 pos = tmp.position;
        tmp.SetParent(null);

        base.ChangeDirection();

        tmp.SetParent(transform);
        tmp.position = pos;
    }

    public void PlayAudioOnce(AudioClip clip, float volume)
    {
        audioSource.volume = volume;
        audioSource.PlayOneShot(clip);
    }

    private void PlayStepSound()
    {
       PlayAudioOnce(step2Sound, 0.6f);
    }

    private void PlayAttackSound()
    {
        PlayAudioOnce(attackSound, 0.5f);
    }

    public void PlayThrowSound()
    {
        PlayAudioOnce(throwSound, 1);
    }

    public void PlayDamageSound()
    {
        PlayAudioOnce(damageSound, 0.3f);
    }

    public IEnumerator PlayCrySound()
    {
        PlayAudioOnce(ravenCrySound, 0.8f);
        yield return new WaitForSeconds(10);
    }

}
