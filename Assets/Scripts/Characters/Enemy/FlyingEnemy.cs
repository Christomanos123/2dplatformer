﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour {
    //patrol, chase, attack, death

    [SerializeField] public FlyingEnemyStates eCurrentState = FlyingEnemyStates.Idle;
    public enum FlyingEnemyStates
    {
        Patrol,
        Chase,
        Attack,
        Idle,
    }

    private bool isDead
    {
        get
        {
            return m_HealthStat.CurrentValue <= 0;
        }
    }

    [SerializeField] private Stat m_HealthStat;

    private Animator m_Animator;
    private SpriteRenderer m_SpriteRenderer;

    [SerializeField] private Transform m_LeftEdge;
    [SerializeField] private Transform m_RightEdge;

    [SerializeField] private float m_MovementSpeed;
    [SerializeField] private bool m_IsFacingRight;

    private float idleTimer = 0;
    private float idleDuration;

    private float patrolTimer = 0;
    private float patrolDuration;

    [SerializeField] private GameObject m_KnifePrefab;
    [SerializeField] private Transform m_KnifePos;

    private bool m_CanThrow;

    private float m_CanThrowTimer = 0;
    [SerializeField] private float m_CanThrowDuration;



    // Use this for initialization
    void Start () {
        m_IsFacingRight = false;
        m_Animator = GetComponent<Animator>();
        idleDuration = Random.Range(1, 10);
        patrolDuration = Random.Range(1, 10);

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!isDead)
        {
            updateStates();
        }
	}

    private void updateStates()
    {
        switch(eCurrentState)
        {
            case FlyingEnemyStates.Patrol:
                {
                    HandlePatrolState();
                    break;
                }
            case FlyingEnemyStates.Attack:
                {
                    
                    break;
                }
            case FlyingEnemyStates.Idle:
                {
                    HandleIdleState();
                    break;
                }
        }
    }

    private void FixedUpdate()
    {
        HandleAttackState();
    }


    private void HandlePatrolState()
    {
        if ((GetDirection().x < 0 && transform.position.x > m_LeftEdge.position.x) || (GetDirection().x > 0 && transform.position.x < m_RightEdge.position.x))
        {
            m_Animator.SetFloat("speed", 1);
            transform.Translate(GetDirection() * (m_MovementSpeed * Time.deltaTime));
            patrolTimer += Time.deltaTime;
            if (patrolTimer >= patrolDuration)
            {
                eCurrentState = FlyingEnemyStates.Idle;
                patrolTimer = 0;
            }
        }
        else
        {
            ChangeDirection();
        }


    }

    private void HandleIdleState()
    {

        m_Animator.SetFloat("speed", 0);
        idleTimer += Time.deltaTime;

        if (idleTimer >= idleDuration)
        {
            eCurrentState = FlyingEnemyStates.Patrol;
            idleTimer = 0;
        }

    }

    private void HandleAttackState()
    {
        int layerMask = 1 << 11;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 50.0f, layerMask);

        if (hit.collider != null)
        {
            Debug.Log("Found an object - distance: " + hit.distance);
            if (m_CanThrow)
            {
                DropKnife();
                m_CanThrow = false;
            }
        }
        m_CanThrowTimer += Time.deltaTime;
        if (m_CanThrowTimer >= m_CanThrowDuration)
        {
            m_CanThrow = true;
            m_CanThrowTimer = 0;
        }


    }

    //Generate knife
   public void DropKnife()
    {
       GameObject tmp = (GameObject)Instantiate(m_KnifePrefab, m_KnifePos.position, Quaternion.Euler(0, 0, -90));
       tmp.GetComponent<Knife>().Initialize(Vector2.down);  
    }



    public Vector2 GetDirection()
    {
        return m_IsFacingRight ? Vector2.right : Vector2.left;
    }

    public void ChangeDirection()
    {
        m_IsFacingRight = !m_IsFacingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

}
