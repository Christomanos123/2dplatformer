﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingObject : MonoBehaviour
{

    [SerializeField] private float horizontalSpeed, verticalSpeed, amplitude;

    private Vector2 tempPosition;

	
	void Start ()
    {
        tempPosition = transform.position;
	}
	
	
	void Update ()
    {
        tempPosition.x += horizontalSpeed;
        tempPosition.y = Mathf.Sin(Time.realtimeSinceStartup * verticalSpeed) * amplitude;
        transform.position = tempPosition;
	}
}
