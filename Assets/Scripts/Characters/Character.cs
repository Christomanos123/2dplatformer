﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Character : MonoBehaviour {

    public Animator MyAnimator { get; private set; }

    [SerializeField] protected float movementSpeed;
    [SerializeField] protected Stat healthStat;

    protected bool facingRight;
    public bool Attack { get; set; }
    public abstract void Death();

    public Stat HealthStat
    {
        get
        {
            return healthStat;
        }

        set
        {
            healthStat = value;
        }
    }

     protected SpriteRenderer spriteRenderer;

    [SerializeField] private List<string> damageSources; 
    public abstract bool IsDead { get; }
    public bool TakingDamage { get; set; }

    [SerializeField] protected Transform knifePos;
    [SerializeField] private GameObject knifePrefab;

    protected bool m_IsAttackedByBoss;

    [SerializeField] private EdgeCollider2D swordCollider;
    public EdgeCollider2D SwordCollider
    {
        get
        {
            return swordCollider;
        }
    }


    // Use this for initialization
    public virtual void Start () {
        facingRight = true;
        MyAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        healthStat.Initilize();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MeleeAttack()
    {
        SwordCollider.enabled = true;
    }


    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (damageSources.Contains(other.tag))
        {
            if (other.CompareTag("BossSword"))
            {
                m_IsAttackedByBoss = true;
            }
            StartCoroutine(TakeDamage());   
        }    
    }

    

    public virtual void ChangeDirection()
    {
        facingRight = !facingRight;
        // spriteRenderer.flipX = !spriteRenderer.flipX;
       
       transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }



    public virtual void ThrowKnife(int value)
    {
        if (facingRight)
        {
            GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePos.position, Quaternion.Euler(0, 0, 0));
            tmp.GetComponent<Knife>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePos.position, Quaternion.Euler(0, 0, 180));
            tmp.GetComponent<Knife>().Initialize(Vector2.left);
        }
    }

    public abstract IEnumerator TakeDamage();



}
