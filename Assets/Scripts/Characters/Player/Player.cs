﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DeadHandlerEvent();


public class Player : Character
{
    private static Player instance;
    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Player>();
            }
            return instance;
        }

        
    }
    //Audio Effects
    [SerializeField] AudioSource audioSource;

    //Audio clip one shot
    [SerializeField] AudioClip throwSound;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip deathSound;
    [SerializeField] AudioClip jumpSound;
    [SerializeField] AudioClip damageSound;
    [SerializeField] AudioClip step2Sound;
    [SerializeField] AudioClip ladderStepSound;
    [SerializeField] AudioClip landSound;
    [SerializeField] AudioClip slideSound;

    //Audio looping clips
    [SerializeField] AudioClip runSound;

    public Rigidbody2D MyRigidbody2D { get; set; }
    //private SpriteRenderer spriteRenderer;

    //Ground related
    [SerializeField] private Transform[] groundPoints;
    [SerializeField] private float groundRadius;
    [SerializeField] private LayerMask whatIsGround;


    //Jump
    [SerializeField] private float jumpForce;
    [SerializeField] private bool airControl;
    
    //Use
    private IUseable useable;
    [SerializeField] private float climbSpeed;
    public bool OnLadder { get; set; }
    
    //Immortal
    [SerializeField] private float immortalTime;
    private bool immortal = false;

    //Checkpoint
    private Vector2 respawnPoint;
    private bool checkpointReached = false;
    private Vector3 startPos;

    //Attack
    private float attackTimer;
    private float attackCoolDown = 0.5f;
    private bool canAttack = true;


    public bool Slide { get; set; }
    public bool Jump { get; set; }
    public bool OnGround { get; set; }
    public bool AllowedToMove { get; set; }

   
    public DeadHandlerEvent DeadEvent;

   



    public override void Start ()
    {
        AllowedToMove = true;
        base.Start();
        OnLadder = false;
        
        MyRigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        startPos = transform.position;
	}

    private void Update()
    {
            if (!IsDead)
            {
                HandleAttack();
                if (!TakingDamage)
                {
                    HandleInput();
                }
            }
    }
    
    void FixedUpdate () {
        if (!TakingDamage && !IsDead)
        {

            OnGround = IsGrounded();

            if (AllowedToMove)
            {
                float horizontal = Input.GetAxis("Horizontal");
                float vertical = Input.GetAxis("Vertical");
                HandleMovement(horizontal, vertical);
                Flip(horizontal);
               
            }
            
            HandleLayers();
        }
	}


    public override bool IsDead
    {
        get
        {
            if (healthStat.CurrentValue <= 0 )
            {
                OnDead();
            }
           

            return healthStat.CurrentValue <= 0;
        }
      

    }
    public void OnDead()
    {
        if (DeadEvent != null)
        {
            DeadEvent();
        }
    }
    public override void Death()
    {
        MyAnimator.SetFloat("speed", 0);
        MyRigidbody2D.velocity = Vector2.zero;
        
        if (checkpointReached)
        {
            transform.position = respawnPoint;
        }
        else
        {
            transform.position = startPos;
        }
        MyAnimator.SetTrigger("idle");
        healthStat.CurrentValue = healthStat.MaxValue;
        GameManager.Instance.Health = healthStat.CurrentValue;
    }

    private void HandleMovement(float horizontal,float vertical)
    {
        //Land
        if (MyRigidbody2D.velocity.y < 0)
        {
            MyAnimator.SetBool("land", true);
        }

        //Move
        if (!Attack && !Slide && (OnGround || airControl))
        {
            MyRigidbody2D.velocity = new Vector2(horizontal * movementSpeed, MyRigidbody2D.velocity.y);
        }

        if (Jump && MyRigidbody2D.velocity.y == 0 && !OnLadder)
        {
            MyRigidbody2D.AddForce(new Vector2(0, jumpForce));
            // audioSource.PlayOneShot(audioClips[Random.Range(0,audioClips.Length)]);
           // PlayAudioOnce(jumpSound,0.7f);
        }

        if (OnLadder)
        {
            MyAnimator.speed = vertical != 0 ? Mathf.Abs(vertical) : Mathf.Abs(horizontal);
            MyRigidbody2D.velocity = new Vector2(horizontal * climbSpeed, vertical * climbSpeed);
        }

        MyAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }

    public void PlayAudioOnce(AudioClip clip, float volume)
    {
        audioSource.volume = volume;
        audioSource.PlayOneShot(clip);
    }

    public void PlayThrowSound()
    {
        PlayAudioOnce(throwSound, 1);
    }

    public void PlayLandSound()
    {
        PlayAudioOnce(landSound,1);
    }

    private void PlayJumpSound()
    {
        PlayAudioOnce(jumpSound, 0.7f);
    }

    private void PlayStepSound()
    {
        if (OnGround)
        {
            PlayAudioOnce(step2Sound,1);
        }
    }

    private void PlayAttackSound()
    {
        PlayAudioOnce(attackSound, 1);
    }

    private void PlayLadderStepSound()
    {
        PlayAudioOnce(ladderStepSound, 1);
    }

    private void HandleInput()
    {

        //Slide
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MyAnimator.SetTrigger("slide");
            PlayAudioOnce(slideSound,1);
        }

        //Jump
        if (Input.GetKey(KeyCode.UpArrow) && OnGround && !OnLadder)
        {
            MyAnimator.SetTrigger("jump");
        }

        //Throw
        if (Input.GetKeyDown(KeyCode.Z))
        {
            MyAnimator.SetTrigger("throw");
           
        }

        //Use
        if (Input.GetKeyDown(KeyCode.C))
        {
            Use();
        }
        
    }

    private void HandleAttack()
    {
         attackTimer += Time.deltaTime;
         if (attackTimer >= attackCoolDown)
          {
              canAttack = true;
               attackTimer = 0;
          }

        //Attack
        if (Input.GetKeyDown(KeyCode.X) && canAttack)
        {
                MyAnimator.SetTrigger("attack");
                MeleeAttack();
                canAttack = false;
            
        }
    }



    public override void ThrowKnife(int value)
    {
        if (!OnGround && value == 1 || OnGround && value == 0)
        {
            base.ThrowKnife(value);
        }
    }

    
    private void Flip(float horizontal)
    {
        if (!this.MyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
        {
            if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
            {
                ChangeDirection();
            }
        }
    }

    private bool IsGrounded()
    {
        if (MyRigidbody2D.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius,whatIsGround);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject && !colliders[i].isTrigger)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    //Switch between ground and air layer
    private void HandleLayers()
    {
        if (!OnGround)
        {
            MyAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            MyAnimator.SetLayerWeight(1, 0);
        }
    }

    private IEnumerator IndicateImmortal()
    {
        while (immortal)
        {
            spriteRenderer.enabled = false;

            yield return new WaitForSeconds(0.1f);

            spriteRenderer.enabled = true;

            yield return new WaitForSeconds(0.1f);
        }
    }


    //We use IEnumerator so we can use the time so the player can blink
    public override IEnumerator TakeDamage()
    {
        if (!immortal)
        {
            if (m_IsAttackedByBoss)
            {
                healthStat.CurrentValue -= 20;
                m_IsAttackedByBoss = false;
            }
            else
                healthStat.CurrentValue -= 10;

            GameManager.Instance.Health -= 10;
            if (!IsDead)
            {
                MyAnimator.SetTrigger("damage");
                PlayAudioOnce(damageSound,0.2f);
                immortal = true;
                StartCoroutine(IndicateImmortal());
                yield return new WaitForSeconds(immortalTime);

                immortal = false;
            }
            else
            {
                MyAnimator.SetLayerWeight(1, 0);
                MyAnimator.SetTrigger("die");
                PlayAudioOnce(deathSound,0.5f);
            }


        }
    }
    
    private void Use()
    {
        if (useable != null)
        {
            useable.Use();
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Useable"))
        {
            //Get IUseable component from the useable object
            useable = collision.GetComponent<IUseable>();
        }

        if (collision.gameObject.CompareTag("PickUp"))
        {
            collision.gameObject.GetComponent<PickUp>().OnCollision();
        }

        if (collision.CompareTag("Checkpoint"))
        {
            checkpointReached = true;
            respawnPoint = collision.transform.position;
        }

        base.OnTriggerEnter2D(collision);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Useable"))
        {
            useable = null;
        }
    }

}
