﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class Fireball : MonoBehaviour {

    [SerializeField] private float speed;

    private Vector2 direction;

    private Animator animator;
    

    private Rigidbody2D rigidbody2D;

	// Use this for initialization
	void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = direction * speed;
    }

    public void Initialize(Vector2 direction)
    {
        this.direction = direction;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Knife") || collision.CompareTag("Player") || collision.CompareTag("Sword"))
        {
            animator.SetTrigger("die");
            Destroy(gameObject, 0.6f);
        }
    }


    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
